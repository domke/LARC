#!/bin/bash

export ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR
source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/env.cfg
load_compiler_env "$1"

BM="SW4lite"
VERSION="ca73af2cf85c22acfc203cbb5a1d3650bc874361"	#old: "5ab8063ecdc94bdb59a5e65396c85bd54f9e0916"
HHOST="${XEONHOST}${IKNLHOST}${IKNMHOST}${FUJIHOST}${M100HOST}${PS5CHOST}${MILXHOST}"; if [ -z "${HHOST}" ] && [ -n "${GEM5HOST}" ]; then HHOST="fugaku"; fi
if [[ "$2" = *"rebuild"* ]]; then rm -rf $BM .git/modules/$BM; git submodule update --init $BM; fi
if [ ! -f $ROOTDIR/$BM/optimize_mp_${HHOST}/sw4lite ]; then
	cd $ROOTDIR/$BM/
	if ! [[ "$(git rev-parse --abbrev-ref HEAD)" = *"precision"* ]]; then git checkout -b precision ${VERSION}; fi
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	instrument_kernel "$1" $ROOTDIR/$BM/
	MAKEFILE=Makefile
	sed -i -e "s/^HOSTNAME := /HOSTNAME := ${HHOST} #/g" ./${MAKEFILE}
	sed -i -e "s/quadknl/${HHOST}/g" ./${MAKEFILE}
	sed -i -e "s#/opt/intel/compilers_and_libraries_2017/linux#`dirname $MKLROOT`#g"  ./${MAKEFILE}
	if [ -z "${IKNLHOST}" ] && [ -z "${IKNMHOST}" ]; then
		sed -i -e "s/-xmic-avx512/#NOKNL-xmic-avx512/g" ./${MAKEFILE}
	fi
	if [[ "$1" = *"intel"* ]]; then
		sed -i -e 's/mpifort/mpif90/' -e 's/mpiifort/mpif90/' -e 's/mpiicpc/mpicxx/' -e 's/-L${ADVISOR/-static -static-intel -qopenmp-link=static -L${ADVISOR/' -e "s#MKL_PATH = .*#MKL_PATH = $MKLROOT/lib/intel64#" -e 's#-lmkl_intel_lp64 -lmkl_core -lmkl_sequential#-Wl,--start-group ${MKL_PATH}/libmkl_intel_lp64.a ${MKL_PATH}/libmkl_sequential.a ${MKL_PATH}/libmkl_core.a -Wl,--end-group#' -e 's/-lintlc/-lirc/' ./${MAKEFILE}
	elif [[ "$1" = *"gnu"* ]]; then
		if [ -n "$FJMPI" ]; then sed -i -e 's/mpifort/mpifrt/' -e 's/mpiifort/mpifrt/' -e 's/mpiicpc/mpiFCC/' -e 's/= icc/= mpifcc/' ./${MAKEFILE};
		else                     sed -i -e 's/mpifort/mpif90/' -e 's/mpiifort/mpif90/' -e 's/mpiicpc/mpicxx/' -e 's/= icc/= gcc/' ./${MAKEFILE}; fi
		if [ -n "$MKLROOT" ]; then
			sed -i -e 's/-ipo -xHost/-march=native -flto/g' -e 's# -I${ADVISOR_2018_DIR}/include# -m64 -I${MKLROOT}/include#g' -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -Wl,--start-group \${MKLROOT}/lib/intel64/libmkl_intel_lp64.a \${MKLROOT}/lib/intel64/libmkl_sequential.a \${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lgfortran -lquadmath -lpthread -lm -ldl -flto ${MAYBESTATIC}#" ./${MAKEFILE}
		elif [ -n "$FJBLAS" ]; then
			sed -i -e 's/-ipo -xHost/-march=native -flto/g' -e "s# -I\${ADVISOR_2018_DIR}/include# -I$(dirname `which fcc`)/../include#g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -L$(readlink -f $(dirname $(which mpifcc))/../lib64) $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjomp.o -lfj90rt2 -lssl2mtexsve -lssl2mtsve -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjprofcore -lfjprofomp -lelf -lgfortran -flto ${MAYBESTATIC}#" ./${MAKEFILE}
		fi
	elif [[ "$1" = *"fujitrad"* ]]; then
		sed -i -e 's/mpifort/mpifrt/' -e 's/mpiifort/mpifrt/' -e 's/mpiicpc/mpiFCC/' -e 's/= icc/= mpifcc/' -e 's/-ipo -xHost/-Kfast,openmp,ocl,largepage,lto/g' -e "s# -I\${ADVISOR_2018_DIR}/include##g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -SSL2BLAMP#g" ./${MAKEFILE}
	elif [[ "$1" = *"fujiclang"* ]]; then
		sed -i -e 's/mpifort/mpifrt/' -e 's/mpiifort/mpifrt/' -e 's/mpiicpc/mpiFCC/' -e 's/= icc/= mpifcc/' -e 's/FORT_FLAGS = -ipo -xHost/FORT_FLAGS = -Nclang -mcpu=a64fx+sve -fopenmp -Kopenmp -Kfast,ocl,largepage,lto/g' -e 's/-ipo -xHost/-Nclang -Ofast -mcpu=a64fx+sve -fopenmp -ffj-ocl -ffj-largepage/g' -e "s# -I\${ADVISOR_2018_DIR}/include##g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -SSL2BLAMP#g" ./${MAKEFILE}
	elif [[ "$1" = *"gem5"* ]]; then
		sed -i -e 's/mpifort/frt/' -e 's/mpiifort/frt/' -e 's/mpiicpc/FCC/' -e 's/= icc/= fcc/' -e 's/FORT_FLAGS = -ipo -xHost/FORT_FLAGS = -Nclang -mcpu=a64fx+sve -fopenmp -Kopenmp -Kfast,ocl,nolargepage,nolto/g' -e 's/-ipo -xHost/-Nclang -Ofast -mcpu=a64fx+sve -fopenmp -ffj-ocl -ffj-no-largepage -fno-lto/g' -e "s# -I\${ADVISOR_2018_DIR}/include# -I$ROOTDIR/dep/mpistub/include/mpistub#g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -SSL2BLAMP -Wl,-rpath=$ROOTDIR/dep/mpistub/lib/mpistub -L$ROOTDIR/dep/mpistub/lib/mpistub -lmpi#g" ./${MAKEFILE}
	elif [[ "$1" = *"llvm12"* ]]; then
		sed -i -e 's/mpifort/mpifrt/' -e 's/mpiifort/mpifrt/' -e 's/mpiicpc/mpiFCC/' -e 's/= icc/= mpifcc/' -e 's/FORT_FLAGS = -ipo -xHost/FORT_FLAGS = -mcpu=a64fx+sve -mtune=a64fx+sve -fopenmp -Kopenmp -Kfast,ocl,largepage,lto/g' -e 's/-ipo -xHost/-Ofast -ffast-math -mcpu=a64fx -mtune=a64fx -fopenmp -mllvm -polly -mllvm -polly-vectorizer=polly -flto=full/g' -e "s# -I\${ADVISOR_2018_DIR}/include##g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -fuse-ld=lld -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjomp.o -lfj90rt2 -lssl2mtexsve -lssl2mtsve -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjprofcore -lfjprofomp#g" ./${MAKEFILE}
	elif [[ "$1" = *"llvm13"* ]]; then
		sed -i -e 's/mpifort/flang/' -e 's/mpiifort/flang/' -e 's/mpiicpc/mpiFCC/' -e 's/= icc/= mpifcc/' -e "s#FORT_FLAGS = -ipo -xHost#FORT_FLAGS = -Ofast -ffast-math -mcpu=a64fx -mtune=a64fx -msve-vector-bits=scalable -fopenmp -flto=full $(mpifrt -showme:compile | sed -e 's/ -Knointentopt//')#g" -e 's/-ipo -xHost/-Ofast -ffast-math -mcpu=a64fx -mtune=a64fx -msve-vector-bits=scalable -fopenmp -flto=full/g' -e "s# -I\${ADVISOR_2018_DIR}/include##g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -l:libpgmath.a -l:libflang.a -l:libflangrti.a -lrt -fuse-ld=lld -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjomp.o -lfj90rt2 -lssl2mtexsve -lssl2mtsve -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjprofcore -lfjprofomp#g" ./${MAKEFILE}
	elif [[ "$1" = *"aocc"* ]]; then
		sed -i -e 's/mpiifort/mpif90/' -e 's/mpiicpc/mpicxx/' -e 's/= icc/= mpicc/' -e 's/FORT_FLAGS = -ipo -xHost/FORT_FLAGS = -Ofast -ffast-math -march=native -fopenmp -finline-aggressive -flto/g' -e 's/-ipo -xHost/-Ofast -ffast-math -march=native -fopenmp -finline-aggressive -flto/g' -e "s# -I\${ADVISOR_2018_DIR}/include# -I${AOCL_PATH}/include#g" -e "s#EXTRA_LINK_FLAGS = .*#EXTRA_LINK_FLAGS = -fuse-ld=lld -L$(readlink -f $(dirname $(which mpicc))/../) -lopen-pal -L${AOCL_PATH}/lib -l:libblis-mt.a -l:libflame.a -L$(readlink -f $(dirname $(which clang))/../lib) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) -lpgmath -lflang -lflangrti -lrt -l:libalm.a#g" ./${MAKEFILE}
	elif [[ "$1" = *"cuda"* ]]; then
		BLAS_HOME=`spack find -p ${SPACKARCH} %gcc@8.4.0 | /bin/grep openblas | cut -d' ' -f2- | tr -d ' '`
		LAPACK_HOME=`spack find -p ${SPACKARCH} %gcc@8.4.0 | /bin/grep netlib-lapack | cut -d' ' -f2- | tr -d ' '`
		MAKEFILE=Makefile.cuda
		sed -i -e '/^HOSTNAME/a openmp=yes' -e "s/_surface/_${HHOST}/g" -e 's/$(optdir)_mp/$(optdir)/g' -e 's/sm_60/sm_80/g' -e "s#EXTRA_LINK_FLAGS = -lmpi#EXTRA_LINK_FLAGS = -lmpi -L${LAPACK_HOME}/lib64 -llapack -L${BLAS_HOME}/lib -lopenblas#g" ./${MAKEFILE}
		sed -i -e 's#CHECK_INPUT(false,"The NullFunc#//CHECK_INPUT(false,"The NullFunc#' src/time_functions_cu.C
	elif [[ "$1" =~ (.*rocm.*|.*hip.*) ]]; then
		if [ ! -f ./hip.patch ]; then
			git remote add hip https://github.com/rwvo/sw4lite.git; git fetch hip
			git stash; git checkout hip/hip; git format-patch --stdout 41e9a74~7 > hip.patch
			git checkout precision; git stash pop
		fi
		git apply --check ./hip.patch; if [ "x$?" = "x0" ]; then git am --ignore-whitespace < ./hip.patch; fi
		MAKEFILE=Makefile.hip
		sed -i -e "s#=1 hipcc\$#=1 hipcc $(hipconfig --path)#g" -e "s#^MPIPATH =.*#MPIPATH = $(readlink -f "$(dirname "$(which mpirun)")/../")#g" -e "s/^builddir =.*/builddir = optimize_hip_${HHOST}/g" ./${MAKEFILE}
	fi
	make -f ${MAKEFILE}
	cd $ROOTDIR
fi


#!/bin/bash

export ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR
source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/env.cfg
load_compiler_env "$1"

BM="NPB"
source $ROOTDIR/conf/"$(echo ${BM} | tr '[:upper:]' '[:lower:]')".sh
if [[ "$2" = *"rebuild"* ]]; then rm -rf $BM .git/modules/$BM; git submodule update --init $BM; fi
if ! [ $(find $ROOTDIR/$BM/*/bin -executable -type f | wc -l) -ge 1 ]; then
	mkdir -p $ROOTDIR/$BM/
	cd $ROOTDIR/$BM/
	URL="https://www.nas.nasa.gov/assets/npb/NPB3.4.2.tar.gz"; DEP=$(basename $URL)
	if [ ! -f $ROOTDIR/dep/${DEP} ]; then if ! wget ${URL} -O $ROOTDIR/dep/${DEP}; then echo "ERR: download failed for ${URL}"; exit 1; fi; fi
	tar xzf $ROOTDIR/dep/${DEP} -C $ROOTDIR/$BM --strip-components 1
	if patch --dry-run -s -f -p1 < $ROOTDIR/patches/*1-${BM}*.patch; then
		patch -p1 --forward < $ROOTDIR/patches/*1-${BM}*.patch
	fi
	instrument_kernel "$1" $ROOTDIR/$BM/
	#need this for 1k scal tests
	sed -i -e 's/MAX_NUMBER_OF_TASKS.*/MAX_NUMBER_OF_TASKS 1024/g' $ROOTDIR/$BM/NPB3.4-OMP/DC/adcc.h
	if [[ "$1" = *"intel"* ]]; then
		MPIFC="mpif90"; MPIFFLAGS="-O3 -xHost -static -static-intel -qopenmp-link=static -I${ADVISOR_2018_DIR}/include"; MPIFLINKFLAGS="-L${ADVISOR_2018_DIR}/lib64 -littnotify -L/usr/lib64 -lm -lpthread"
		MPICC="mpicc"; MPICFLAGS="-O3 -xHost -static -static-intel -qopenmp-link=static -I${ADVISOR_2018_DIR}/include"; MPICLINKFLAGS="-L${ADVISOR_2018_DIR}/lib64 -littnotify -L/usr/lib64 -lm -lpthread"
		FC="ifort"; FFLAGS="${MPIFFLAGS} -fopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="icc"; CFLAGS="${MPICFLAGS} -fopenmp"; CLINKFLAGS="${MPICLINKFLAGS}"; UCC="icc -g"
	elif [[ "$1" = *"gnu"* ]]; then
		MPIFC="mpif90"; MPIFFLAGS="-O3 -march=native -flto -funroll-loops -ffast-math -ftree-vectorize"; MPIFLINKFLAGS="-flto ${MAYBESTATIC} -L/usr/lib64 -lm"
		MPICC="mpicc"; MPICFLAGS="-O3 -march=native -flto -funroll-loops -ffast-math -ftree-vectorize"; MPICLINKFLAGS="-flto ${MAYBESTATIC} -L/usr/lib64 -lm"
		FC="gfortran"; FFLAGS="${MPIFFLAGS} -fopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="gcc"; CFLAGS="${MPICFLAGS} -fopenmp"; CLINKFLAGS="${MPICLINKFLAGS}"; UCC="gcc -g"
	elif [[ "$1" = *"fujitrad"* ]]; then
		MPIFC="mpifrt"; MPIFFLAGS="-Kfast,ocl,largepage"; MPIFLINKFLAGS=""
		MPICC="mpifcc"; MPICFLAGS="-Kfast,ocl,largepage"; MPICLINKFLAGS=""
		FC="frt"; FFLAGS="${MPIFFLAGS} -Kopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="fcc"; CFLAGS="${MPICFLAGS} -Kopenmp"; CLINKFLAGS="${MPICLINKFLAGS}"; UCC="fcc -g"
	elif [[ "$1" = *"fujiclang"* ]]; then
		MPIFC="mpifrt"; MPIFFLAGS="-Nclang -Ofast -mcpu=a64fx+sve -ffj-ocl -ffj-largepage -flto"; MPIFLINKFLAGS=""
		MPICC="mpifcc"; MPICFLAGS="-Nclang -Ofast -mcpu=a64fx+sve -ffj-ocl -ffj-largepage -flto"; MPICLINKFLAGS=""
		FC="frt"; FFLAGS="${MPIFFLAGS} -fopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="fcc"; CFLAGS="${MPICFLAGS} -fopenmp"; CLINKFLAGS="${MPICLINKFLAGS}"; UCC="fcc -g"
	elif [[ "$1" = *"gem5"* ]]; then
		MPIFC="mpifrt"; MPIFFLAGS="-Nclang -Kfast,ocl,nolargepage,nolto"; MPIFLINKFLAGS=""
		MPICC="mpifcc"; MPICFLAGS="-Nclang -Ofast -mcpu=a64fx+sve -ffj-ocl -ffj-no-largepage -fno-lto"; MPICLINKFLAGS=""
		FC="frt"; FFLAGS="${MPIFFLAGS} -fopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="fcc"; CFLAGS="${MPICFLAGS} -fopenmp"; CLINKFLAGS="${MPICLINKFLAGS}"; UCC="fcc -g"
	elif [[ "$1" = *"llvm12"* ]]; then
		MPIFC="mpifrt"; MPIFFLAGS="-Kfast,ocl,nolargepage,nolto"; MPIFLINKFLAGS=""
		MPICC="mpifcc"; MPICFLAGS="-Ofast -ffast-math -mcpu=a64fx -mtune=a64fx -mllvm -polly -mllvm -polly-vectorizer=polly -flto=full"; MPICLINKFLAGS=""
		FC="frt"; FFLAGS="${MPIFFLAGS} -Kopenmp"; FLINKFLAGS="${MPIFLINKFLAGS}"; CC="clang"; CFLAGS="${MPICFLAGS} -fopenmp"; CLINKFLAGS="-fuse-ld=lld -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib)"; UCC="clang -g"
	elif [[ "$1" = *"llvm13"* ]]; then
		#XXX: mpi crap is broken as 'expected', but omp works: F90-F-0004-Corrupt or Old Module file
		MAINCFLAG="-Ofast -ffast-math -mcpu=a64fx -mtune=a64fx -flto=full -msve-vector-bits=scalable"
		MAINLFLAG="-fuse-ld=lld -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib)"
		MPIFC="flang"; MPIFFLAGS="${MAINCFLAG} $(mpifrt -showme:compile | sed -e 's/ -Knointentopt//')"; MPIFLINKFLAGS="${MAINLFLAG} $(mpifrt -showme:link | sed -e 's/ -Knointentopt//')"
		MPICC="mpifcc"; MPICFLAGS="${MAINCFLAG}"; MPICLINKFLAGS="${MAINLFLAG}"
		FC="flang"; FFLAGS="${MAINCFLAG} -fopenmp"; FLINKFLAGS="${MAINLFLAG}"; CC="clang"; CFLAGS="${MAINCFLAG} -fopenmp"; CLINKFLAGS="${MAINLFLAG}"; UCC="clang -g"
	elif [[ "$1" = *"aocc"* ]]; then
		MAINCFLAG="-Ofast -ffast-math -march=native -fopenmp -finline-aggressive -flto"
		MAINLFLAG="-fuse-ld=lld -L$(readlink -f $(dirname $(which clang))/../lib) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) -l:libalm.a"
		MPIFC="mpif90"; MPIFFLAGS="${MAINCFLAG}"; MPIFLINKFLAGS="${MAINLFLAG}"
		MPICC="mpicc"; MPICFLAGS="${MAINCFLAG}"; MPICLINKFLAGS="${MAINLFLAG}"
		FC="flang"; FFLAGS="${MAINCFLAG} -fopenmp"; FLINKFLAGS="${MAINLFLAG}"; CC="clang"; CFLAGS="${MAINCFLAG} -fopenmp"; CLINKFLAGS="${MAINLFLAG}"; UCC="clang -g"
	fi
	sed -e "s#^MPIFC.*#MPIFC = ${MPIFC}#g" -e "s#^FFLAGS.*#FFLAGS = ${MPIFFLAGS}#g" -e "s#^FMPI_LIB\s*=#FMPI_LIB = ${MPIFLINKFLAGS} #g" \
	    -e "s#^MPICC.*#MPICC = ${MPICC}#g" -e "s#^CFLAGS.*#CFLAGS = ${MPICFLAGS}#g" -e "s#^CMPI_LIB\s*=#CMPI_LIB = ${MPICLINKFLAGS} #g" \
	    -e "s#^CC.*#CC = ${UCC}#g" NPB3.4-MPI/config/make.def.template > NPB3.4-MPI/config/make.def
	sed -e "s#^FC.*#FC = ${FC}#g" -e "s#^FFLAGS.*#FFLAGS = ${FFLAGS}#g" -e "s#^F_LIB\s*=#F_LIB = ${FLINKFLAGS} #g" \
	    -e "s#^CC.*#CC = ${CC}#g" -e "s#^CFLAGS.*#CFLAGS = ${CFLAGS}#g" -e "s#^C_LIB\s*=#C_LIB = ${CLINKFLAGS} #g" \
	    -e "s#^UCC.*#UCC = ${UCC}#g" NPB3.4-OMP/config/make.def.template > NPB3.4-OMP/config/make.def
	for BMconf in ${BINARYS}; do
		BName="$(echo ${BMconf} | cut -d '|' -f1 | xargs basename)"
		Class="$(echo ${BMconf} | cut -d '|' -f2)"
		BMDIR="$(echo ${BMconf} | rev | xargs basename | rev)"
		cd ${BMDIR} ; make -B ${BName} CLASS=${Class} ; cd -
	done
	find $ROOTDIR/$BM/*/bin -executable -type f -exec echo {} \; -exec ldd {} \;
	cd $ROOTDIR
fi


#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}" "-1"
SELECTEDBM="${2}"; BMNUM="$(echo ${2} | tr -dc '0-9')"; ReqArch="${3}"; PinCore="${4}"
if [[ "${SELECTEDBM}" = "B"* ]] && [ -n "${BMNUM}" ]; then BMCNT=0;
else echo 'ERR: need to specify sub-benchmark via B<number> in 2nd argument'; exit 1; fi

SPEC0CMD="runcpu --ignore_errors --config=nedo.cfg --nobuild --action=run --noreportable --use_submit_for_speed"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOGDIR="log/$(hostname -s)/gem5run/${BenchID}"; RLOGDIR="${ROOTDIR}/${LOGDIR}"; mkdir -p "${LOGDIR}" "${RLOGDIR}"

# check that we have the correct host tools
SysEFL="$(file -b $(which tar) | cut -d ',' -f2 | tr -d '[:space:]')"
if ! [[ "$(file -b ./bin/spectar | cut -d ',' -f2 | tr -d '[:space:]')" = "${SysEFL}" ]]; then
	echo "ERR: spec tools for wrong system detected; replace ./bin folder and ./TOOLS.sha512 files to match this server"
	exit
fi

for BENCH in ${BINARY}; do
	BMCNT=$((1+BMCNT)); if [ ${BMNUM} -ne ${BMCNT} ]; then continue; fi
	BM="$(echo ${BENCH} | cut -d '|' -f1)"
	SIZE="$(echo ${BENCH} | cut -d '|' -f2)"
	SnowflakeNumOMP="$(echo ${BENCH} | cut -d '|' -f3)"
	#XXX: my love for fujitsu needs to be endless
	if [[ "${BM}" =~ (.*\.wrf_.*|.*\.pop2_.*) ]]; then export SNOWFLAKE='yes'; SPECCMD="export FORT90L='-Wl,-T'; ${SPEC0CMD}"; else SPECCMD="${SPEC0CMD}"; fi
	LOG="${LOGDIR}/${BM}/conf${ReqArch}.log"; RLOG="${RLOGDIR}/${BM}/conf${ReqArch}.log"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})" "${LOG}_stat"
	for BEST in ${GEM5CONF}; do
		NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
		NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		#Some run better with pow2 or other OMP configs, so let them...
		if [ -n "${SnowflakeNumOMP}" ]; then NumOMP="${SnowflakeNumOMP}"; fi
		#For SPECspeed Integer, only the compression benchmark 657.xz_s includes OpenMP, but also does not scale with threads.
		if [[ "${BM}" =~ (.*\.perlbench_.*|.*\.gcc_.*|.*\.mcf_.*|.*\.omnetpp_.*|.*\.xalancbmk_.*|.*\.x264_.*|.*\.deepsjeng_.*|.*\.leela_.*|.*\.exchange2_.*|.*\.xz_.*) ]]; then NumOMP=1; fi
		echo -e "=== runing ${BENCH} ===\nsource ./shrc; ${SPECCMD} --iterations=${NumRunsGEM5} --size=${SIZE} --threads=${NumOMP} --define COMP=${COMP} --define RESDIR=$(readlink -f ${LOG}_stat) ${BM}" >> ${LOG} 2>&1
		SPEC_WL_NR=dummy cat <<EOF > ${LOG}_stat/gem5wrap.sh
#!/bin/bash
source ${ROOTDIR}/conf/env.cfg
eval "spec_parse_cmd \"\$@\""
###echo -e "\$SPEC_WL_NR\n\$BINARY\n\$ARGS\n\$OUTPUT\n\$ERROR\n\$STRIPPEDCMD" >/dev/shm/1
if [[ "\$BINARY" = *"SPEC_CPU/bin/spec"* ]]; then
eval "\$STRIPPEDCMD"; exit "\$?"
else
echo -e "+ benchspec/CPU/${BM}/\n- benchspec/CPU/*" > "$(pwd)/${LOG}_stat/rsync.filter"
$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "${SNOWFLAKE}" "${ReqArch}" "") "\$BINARY" -o "\$ARGS" -i "\$INPUT" >> $(readlink -f ${LOG}) 2>&1 &
prep_gem5_checkpointrestart "\$!" "$(pwd)" "${APPROOT}" "$(pwd)/${LOG}_stat/rsync.filter" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; RETCODE="\$?"; rsync -aHAX "$(pwd)/${LOG}"* "$(dirname ${RLOG})"; unset SPEC_WL_NR; exit \${RETCODE}
fi
EOF
		bash -c "source ./shrc; ${SPECCMD} --iterations=${NumRunsGEM5} --size=${SIZE} --threads=${NumOMP} --define COMP=${COMP} --define RESDIR=$(readlink -f ${LOG}_stat) ${BM}" >> ${LOG} 2>&1
		if [[ "${BM}" =~ (.*\.perlbench_.*|.*\.gcc_.*|.*\.mcf_.*|.*\.omnetpp_.*|.*\.xalancbmk_.*|.*\.x264_.*|.*\.deepsjeng_.*|.*\.leela_.*|.*\.exchange2_.*|.*\.xz_.*) ]]; then break; fi
	done
done
cd ${ROOTDIR}

#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
LOGDIR="${ROOTDIR}/log/$(hostname -s)/fapprun/${BenchID}"
mkdir -p ${LOGDIR}
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"

for BEST in ${BESTCONF}; do
	for BMconf in ${BINARYS}; do
		NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
		NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		BINARY="$(echo ${BMconf} | tr '|' '_')"
		BName="$(basename ${BINARY})"
		LOG="${LOGDIR}/${BName}.log"
		echo "OMP_NUM_THREADS=${NumOMP} timeout --kill-after=30s ${MAXTIME} ${BINARY} ${INPUT}" >> ${LOG} 2>&1
		rm -rf tmp*/
		for i in $(seq 1 17); do
			OMP_NUM_THREADS=${NumOMP} timeout --kill-after=30s ${MAXTIME} fapp -C -d ./tmp${i} -Icpupa,nompi -Hevent=pa${i} ${BINARY} ${INPUT} >> ${LOG} 2>&1
			if [ "x$?" = "x124" ] || [ "x$?" = "x137" ]; then echo "Killed after exceeding ${MAXTIME} timeout" >> ${LOG} 2>&1; fi
		done
		mkdir -p "${LOGDIR}/${BName}.fapp.report"
		for i in `seq 1 17`; do fapp -A -d ./tmp${i}  -Icpupa -ttext -o "${LOGDIR}/${BName}.fapp.report/pa${i}.txt"; done
		for i in `seq 1 17`; do fapp -A -d ./tmp${i}  -Icpupa -tcsv -o "${LOGDIR}/${BName}.fapp.report/pa${i}.csv"; done
		for i in `seq 1 17`; do fapp -A -d ./tmp${i}  -Icpupa -txml -o "${LOGDIR}/${BName}.fapp.report/pa${i}.xml"; done
	done
done
cd ${ROOTDIR}
echo -e "Do this on login node:\nexport REPXML=\"\$(dirname \"\$(which fapppx)\")/../misc/cpupa/cpu_pa_report.xlsm\"; find ${LOGDIR} -type d -name '*.report' -exec cp \${REPXML} {} \\;"

#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}" "-1"
SELECTEDBM="${2}"; BMNUM="$(echo ${2} | tr -dc '0-9')"; ReqArch="${3}"; PinCore="${4}"
if [[ "${SELECTEDBM}" = "B"* ]] && [ -n "${BMNUM}" ]; then BMCNT=0;
else echo 'ERR: need to specify sub-benchmark via B<number> in 2nd argument'; exit 1; fi

SPECCMD="runspec --ignore_errors --config=nedo.cfg --nobuild --action=run --noreportable"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOGDIR="log/$(hostname -s)/gem5run/${BenchID}"; RLOGDIR="${ROOTDIR}/${LOGDIR}"; mkdir -p "${LOGDIR}" "${RLOGDIR}"

# check that we have the correct host tools
SysEFL="$(file -b $(which tar) | cut -d ',' -f2 | tr -d '[:space:]')"
if ! [[ "$(file -b ./bin/spectar | cut -d ',' -f2 | tr -d '[:space:]')" = "${SysEFL}" ]]; then
	echo "ERR: spec tools for wrong system detected; replace ./bin folder and ./SUMS.tools files to match this server"
	echo "     and run this crap:  PerlPath=\"\$(readlink -f bin/specperl)\" ; for FILE in \$(grep '^#\!.*/specperl' -r bin/ | cut -d ':' -f1 | sort -u) ; do sed -i -e \"s@^#\\!.*/specperl@#\\!\${PerlPath}@g\" \${FILE} ; done; for FILE in \$(grep '^#\!.*/specperl' -r bin/ | cut -d ':' -f1 | sort -u) ; do MD5=\"\$(md5sum \${FILE} | cut -d' ' -f1)\"; sed -i -E \"s@[0-9a-z]*(.* \${FILE})\\\$@\${MD5}\\\\1@g\" SUMS.tools; done; for FILE in \$(grep '^#\!.*/specperl' -r bin/ | cut -d ':' -f1 | sort -u) ; do MD5=\"\$(md5sum \${FILE} | cut -d' ' -f1)\"; sed -i -E \"s@[0-9a-z]*(.* \${FILE})\\\$@\${MD5}\\\\1@g\" MANIFEST; done"
	exit
fi

for BENCH in ${BINARY}; do
	BMCNT=$((1+BMCNT)); if [ ${BMNUM} -ne ${BMCNT} ]; then continue; fi
	BM="$(echo ${BENCH} | cut -d '|' -f1)"
	SIZE="$(echo ${BENCH} | cut -d '|' -f2)"
	SnowflakeNumOMP="$(echo ${BENCH} | cut -d '|' -f3)"
	LOG="${LOGDIR}/${BM}/conf${ReqArch}.log"; RLOG="${RLOGDIR}/${BM}/conf${ReqArch}.log"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})" "${LOG}_stat"
	for BEST in ${GEM5CONF}; do
		NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
		NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		if [ -n "${SnowflakeNumOMP}" ]; then NumOMP="${SnowflakeNumOMP}"; fi
		echo -e "=== runing ${BENCH} ===\nsource ./shrc; ${SPECCMD} --iterations=${NumRunsGEM5} --size=${SIZE} --threads=${NumOMP} --define COMP=${COMP} --define RESDIR=$(readlink -f ${LOG}_stat) ${BM}" >> ${LOG} 2>&1
		SPEC_WL_NR=dummy cat <<EOF > ${LOG}_stat/gem5wrap.sh
#!/bin/bash
source ${ROOTDIR}/conf/env.cfg
eval "spec_parse_cmd \"\$@\""
###echo \"\$@\" >/dev/shm/a
###ls -la /proc/\$\$/fd/ >> /dev/shm/a
###env >> /dev/shm/a
# 363.swim is a special snowflake and has input which is IO redirected by specinvoke
if [[ "\$BINARY" = *"/swim_"*".gem5"* ]] && [ -f /proc/\$\$/fd/0 ] ; then
	INPUT="\$(readlink -f /proc/\$\$/fd/0)"
	OUTPUT="\$(readlink -f /proc/\$\$/fd/1)"
	ERROR="\$(readlink -f /proc/\$\$/fd/2)"
fi
echo -e "+ benchspec/OMP2012/${BM}/\n- benchspec/OMP2012/*" > $(pwd)/${LOG}_stat/rsync.filter
$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "") "\$BINARY" -o "\$ARGS" -i "\$INPUT" >> $(readlink -f ${LOG}) 2>&1 &
prep_gem5_checkpointrestart "\$!" "$(pwd)" "${APPROOT}" "$(pwd)/${LOG}_stat/rsync.filter" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; RETCODE="\$?"; rsync -aHAX "$(pwd)/${LOG}"* "$(dirname ${RLOG})"; unset SPEC_WL_NR; exit \${RETCODE}
EOF
		bash -c "source ./shrc; ${SPECCMD} --iterations=${NumRunsGEM5} --size=${SIZE} --threads=${NumOMP} --define COMP=${COMP} --define RESDIR=$(readlink -f ${LOG}_stat) ${BM}" >> ${LOG} 2>&1
	done
done
cd ${ROOTDIR}

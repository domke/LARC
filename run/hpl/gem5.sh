#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"
ReqArch="${2}"; PinCore="${3}"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOG="log/$(hostname -s)/gem5run/${BenchID}/conf${ReqArch}.log"; RLOG="${ROOTDIR}/${LOG}"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})"

if [ ! -f ./HPL.dat.bak ]; then cp ./HPL.dat ./HPL.dat.bak; fi
for BEST in ${GEM5CONF}; do
	NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
	NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
	MPIP="$(echo ${BEST} | cut -d '|' -f3)"
	MPIQ="$(echo ${BEST} | cut -d '|' -f4)"
	sed -e "s/PNS/${HPLNS}/" -e "s/PNB/${HPLNB}/" -e "s/PPP/${MPIP}/" -e "s/PPQ/${MPIQ}/" ./HPL.dat.bak > ./HPL.dat
	echo "$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") \"${BINARY}\" -o \"${INPUT}\"" >> ${LOG} 2>&1
	for i in $(seq 1 ${NumRunsGEM5}); do
		START="$(date +%s.%N)"
		$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") "${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		prep_gem5_checkpointrestart "$!" "$(pwd)" "${APPROOT}" "" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
		ENDED="$(date +%s.%N)"
		cat ./HPL.out >> ${RLOG} 2>&1; rm ./HPL.out
		echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${RLOG} 2>&1
	done
done
echo "Best ${BenchID} run:"
WALLT="$(/bin/grep '^Walltime' ${RLOG} | awk -F 'kernel:' '{print $2}' | sort -g | head -1)"
/bin/grep "${WALLT}\|^${MPIRUNCMD}" ${RLOG} | /bin/grep -B1 "${WALLT}"
echo ""
cd ${ROOTDIR}

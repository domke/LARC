#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"
SELECTEDBM="${2}"; BMNUM="$(echo ${2} | tr -dc '0-9')"; ReqArch="${3}"; PinCore="${4}"
if [[ "${SELECTEDBM}" = "B"* ]] && [ -n "${BMNUM}" ]; then BMCNT=0;
else echo 'ERR: need to specify sub-benchmark via B<number> in 2nd argument'; exit 1; fi

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
DEFINPUT=${INPUT}
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
DEFLOG="log/$(hostname -s)/gem5run/${BenchID}"; RDEFLOG="${ROOTDIR}/${DEFLOG}"; mkdir -p "$(dirname ${DEFLOG})" "$(dirname ${RDEFLOG})"

for BEST in ${GEM5CONF}; do
	for BINARY in ${BINARYS}; do
		BMCNT=$((1+BMCNT)); if [ ${BMNUM} -ne ${BMCNT} ]; then continue; fi
		NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
		NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		S="$(echo ${BINARY} | cut -d '_' -f2)"
		BINARY="$(echo ${BINARY} | cut -d '_' -f1)"
		LOG="${DEFLOG}${S}gb/conf${ReqArch}.log"; RLOG="${ROOTDIR}/${LOG}"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})"
		S=$((S*1024*1024*1024/8))
		INPUT="$(echo ${DEFINPUT} | sed -e "s/SIZE/${S}/")"
		echo "$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "") \"${BINARY}\" -o \"${INPUT}\"" >> ${LOG} 2>&1
		for i in $(seq 1 ${NumRunsGEM5}); do
			START="$(date +%s.%N)"
			$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "") "${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
			prep_gem5_checkpointrestart "$!" "$(pwd)" "${APPROOT}" "" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
			ENDED="$(date +%s.%N)"
			echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${RLOG} 2>&1
		done
	done
done
echo "Best ${BenchID} run:"
WALLT="$(/bin/grep '^Walltime' ${RLOG} | awk -F 'kernel:' '{print $2}' | sort -g | head -1)"
/bin/grep "${WALLT}\|^${MPIRUNCMD}" ${RLOG} | /bin/grep -B1 "${WALLT}"
echo ""
cd ${ROOTDIR}

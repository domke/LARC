#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}" "8"         # no clue but on Fu it needs much bigger stack
ReqArch="${2}"; PinCore="${3}"

moreMPI="-x FORT_FMT_RECL=400"
if [ -n "${FUJIHOST}" ] || [ -n "${RFX7HOST}" ]; then
	#XXX: my love for fujitsu needs to be endless
	moreMPI+=" -x FORT90L='-Wl,-T'"
fi

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOG="log/$(hostname -s)/gem5run/${BenchID}/conf${ReqArch}.log"; RLOG="${ROOTDIR}/${LOG}"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})"

subOMP="$(for C in ${GEM5CONF}; do echo ${C} | cut -d'|' -f2; done | sort -g -u)"
for NumOMP in ${subOMP}; do
	pushd "${APPROOT}/$(echo "${APPDIR}" | sed -e "s#NICAM#omp${NumOMP}#g")"
	# scale down #steps from 11 days to 1 day, and create input data set
	sed -i -e 's/^LSMAX  = 0/LSMAX  = 72/'  ../../test.conf
	make jobshell > /dev/null 2>&1
	if [ -d "${INPUT}" ] && [ -n "${INPUT}" ]; then pushd ${INPUT}; else exit; fi
	ln -s ../../../../bin/nhm_driver .
	ln -s ../../../../data/mnginfo/rl00-prc10.info .
	ln -s ../../../../data/grid/vgrid/vgrid40_24000-600m.dat .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000000 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000001 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000002 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000003 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000004 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000005 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000006 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000007 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000008 .
	ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000009 .
	popd; popd
done

for BEST in ${GEM5CONF}; do
	NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
	NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
	pushd "${APPROOT}/$(echo "${APPDIR}" | sed -e "s#NICAM#omp${NumOMP}#g")/${INPUT}"
	echo "$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "${moreMPI}" "${ReqArch}" "${PinCore}") \"${BINARY}\"" >> ${LOG} 2>&1
	for i in $(seq 1 ${NumRunsGEM5}); do
		START="$(date +%s.%N)"
		$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "${moreMPI}" "${ReqArch}" "${PinCore}") "${BINARY}" >> ${LOG} 2>&1 &
		prep_gem5_checkpointrestart "$!" "$(pwd)" "${APPROOT}" "" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
		ENDED="$(date +%s.%N)"
		cat ./msg.pe00000 >> ${RLOG} 2>&1
		echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${RLOG} 2>&1
	done
	popd
done

# clean up
for NumOMP in ${subOMP}; do
	pushd "${APPROOT}/$(echo "${APPDIR}" | sed -e "s#NICAM#omp${NumOMP}#g")"
	if [ -d "${INPUT}" ] && [ -n "${INPUT}" ]; then rm -rf ${INPUT}; fi
	popd
done

echo "Best ${BenchID} run:"
WALLT="$(/bin/grep '^Walltime' ${RLOG} | awk -F 'kernel:' '{print $2}' | sort -g | head -1)"
/bin/grep "${WALLT}\|^${MPIRUNCMD}" ${RLOG} | /bin/grep -B1 "${WALLT}"
echo ""
cd ${ROOTDIR}

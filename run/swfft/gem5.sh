#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"
ReqArch="${2}"; PinCore="${3}"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOG="log/$(hostname -s)/gem5run/${BenchID}/conf${ReqArch}.log"; RLOG="${ROOTDIR}/${LOG}"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})"

for BEST in ${GEM5CONF}; do
	NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
	NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
	#XXX: NumMPI=1 always valid and cannot run arm CheckDecomp on x64
	## test if Decomposition is valid
	#INSIZE="$(echo ${INPUT} | awk '{print $2}')"
	#$(get_mpi_cmd "1" "1" "/dev/null" "") $(dirname ${BINARY})/CheckDecomposition ${INSIZE} ${INSIZE} ${INSIZE} ${NumMPI} > /dev/null 2>&1
	#if [ "x$?" = "x0" ]; then
	#	echo "$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") \"${BINARY}\" -o \"${INPUT}\"" >> ${LOG} 2>&1
	#else
	#	echo "INVALID Decomposition: $(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") \"${BINARY}\" -o \"${INPUT}\"" >> ${LOG} 2>&1
	#	continue
	#fi
	for i in $(seq 1 ${NumRunsGEM5}); do
		START="$(date +%s.%N)"
		$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") "${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		prep_gem5_checkpointrestart "$!" "$(pwd)" "${APPROOT}" "" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
		ENDED="$(date +%s.%N)"
		echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${RLOG} 2>&1
	done
done
echo "Best ${BenchID} run:"
WALLT="$(/bin/grep '^Walltime' ${RLOG} | awk -F 'kernel:' '{print $2}' | sort -g | head -1)"
/bin/grep "${WALLT}\|^${MPIRUNCMD}" ${RLOG} | /bin/grep -B1 "${WALLT}"
echo ""
cd ${ROOTDIR}

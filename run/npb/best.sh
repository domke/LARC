#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
LOGDIR="${ROOTDIR}/log/$(hostname -s)/bestrun/${BenchID}"
mkdir -p ${LOGDIR}
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"

for BEST in ${BESTCONF}; do
	for BMconf in ${BBINARYS}; do
		BINARY="$(echo ${BMconf} | cut -d '|' -f-2 | tr '|' '.').x"
		INPUT="$(echo ${BMconf} | cut -d '|' -f5)"
		NumMPI="$(echo ${BMconf} | cut -d '|' -f3)"; if skip_conf "${NumMPI}"; then continue; fi
		NumOMP="$(echo ${BMconf} | cut -d '|' -f4)"
		if   [ "x${NumMPI}"  = "x1" ] && [[ "${BINARY}" = *"-MPI/"* ]]; then continue;
		elif [ "x${NumMPI}" != "x1" ] && [[ "${BINARY}" = *"-OMP/"* ]]; then continue; fi
		BName="$(basename ${BINARY})"
		LOG="${LOGDIR}/${BName}.log"
		echo "$(get_mpi_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "") ${BINARY} ${INPUT}" >> ${LOG} 2>&1
		for i in $(seq 1 ${NumRunsBEST}); do
			START="$(date +%s.%N)"
			timeout --kill-after=30s ${MAXTIME} $(get_mpi_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "") ${BINARY} ${INPUT} >> ${LOG} 2>&1
			clenup_after_mpi_cmd
			if [ "x$?" = "x124" ] || [ "x$?" = "x137" ]; then echo "Killed after exceeding ${MAXTIME} timeout" >> ${LOG} 2>&1; fi
			ENDED="$(date +%s.%N)"
			echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${LOG} 2>&1
			rm -f ADC.*
		done
	done
done
for BMconf in ${BBINARYS}; do
	BName="$(basename "$(echo ${BMconf} | cut -d '|' -f-2 | tr '|' '.').x")"
	LOG="${LOGDIR}/${BName}.log"
	echo "Best ${BName} run:"
	WALLT="$(/bin/grep '^Walltime' ${LOG} | awk -F 'kernel:' '{print $2}' | sort -g | head -1)"
	/bin/grep "${WALLT}\|^${MPIRUNCMD}" ${LOG} | /bin/grep -B1 "${WALLT}"
	echo ""
done
cd ${ROOTDIR}

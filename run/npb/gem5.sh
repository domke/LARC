#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg "${1}"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"
SELECTEDBM="${2}"; BMNUM="$(echo ${2} | tr -dc '0-9')"; ReqArch="${3}"; PinCore="${4}"
if [[ "${SELECTEDBM}" = "B"* ]] && [ -n "${BMNUM}" ]; then BMCNT=0;
else echo 'ERR: need to specify sub-benchmark via B<number> in 2nd argument'; exit 1; fi

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"
LOGDIR="log/$(hostname -s)/gem5run/${BenchID}"; RLOGDIR="${ROOTDIR}/${LOGDIR}"; mkdir -p "${LOGDIR}" "${RLOGDIR}"

for BEST in ${GEM5CONF}; do
	for BMconf in ${BBINARYS}; do
		BMCNT=$((1+BMCNT)); if [ ${BMNUM} -ne ${BMCNT} ]; then continue; fi
		if [[ "${BINARY}" = *"-MPI/"* ]]; then continue; fi
		BINARY="$(echo ${BMconf} | cut -d '|' -f-2 | tr '|' '.').x"
		NumMPI="$(echo ${BEST} | cut -d '|' -f1)"
		NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		SnowflakeNumOMP="$(echo ${BMconf} | cut -d '|' -f4 | sed 's/[^0-9]*//g')"; if [ -n "${SnowflakeNumOMP}" ]; then NumOMP="${SnowflakeNumOMP}"; fi
		BName="$(basename ${BINARY})"
		LOG="${LOGDIR}/${BName}/conf${ReqArch}.log"; RLOG="${RLOGDIR}/${BName}/conf${ReqArch}.log"; mkdir -p "$(dirname ${LOG})" "$(dirname ${RLOG})"
		echo "$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") \"${BINARY}\" -o \"${INPUT}\"" >> ${LOG} 2>&1
		for i in $(seq 1 ${NumRunsGEM5}); do
			START="$(date +%s.%N)"
			$(get_gem5_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "" "${ReqArch}" "${PinCore}") "${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
			prep_gem5_checkpointrestart "$!" "$(pwd)" "${APPROOT}" "" "${LOG}" "${RLOG}" "${RLOG}.criu"; wait; rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
			ENDED="$(date +%s.%N)"
			echo "Total running time: $(echo "${ENDED} - ${START}" | bc -l)" >> ${RLOG} 2>&1
		done
		rm -f ADC.*
	done
done
cd ${ROOTDIR}

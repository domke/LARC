#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
BenchID="$(basename $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) )"
cd ${ROOTDIR}

source ${ROOTDIR}/conf/host.cfg
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "${1}"
maybe_submit_job "${COMP}" "${SELF}" "${ROOTDIR}/conf/${BenchID}.sh"
load_compiler_env "${COMP}"

source ${ROOTDIR}/conf/${BenchID}.sh "${COMP}"
LOGDIR="${ROOTDIR}/log/$(hostname -s)/profrun/${BenchID}"
mkdir -p ${LOGDIR}
move_to_scratch_area "${ROOTDIR}" "${APPDIR}"

export PATH=$ROOTDIR/dep/sde-external-8.35.0-2019-03-11-lin:$PATH
if [ ! -x "`which sde64 2>/dev/null`" ]; then echo "ERROR: SDE missing, please download from Intel sde-external-8.35.0-2019-03-11-lin.tar.bz2 and untar in ./dep folder"; exit; fi;
SDE="`which sde64` -sse-sde -disasm_att 1 -dcfg 1 -dcfg:write_bb 1 -dcfg:out_base_name dcfg-out.rank-\"\$PMIX_RANK\" -align_checker_prefetch 0 -align_correct 0 -emu_fast 1 -start_ssc_mark 111:repeat -stop_ssc_mark 222:repeat"
if [[ $HOSTNAME = *"${XEONHOST}"* ]]; then
	SDE="$SDE -bdw -- "
elif [[ $HOSTNAME = *"${IKNLHOST}"* ]]; then
	SDE="$SDE -knl -- "
elif [[ $HOSTNAME = *"${IKNMHOST}"* ]]; then
	SDE="$SDE -knm -- "
else
	echo "Unsupported host"
	exit
fi
export PATH=$ROOTDIR/dep/intel-pcm:$PATH
PCMB="pcm.x pcm-memory.x pcm-power.x"
VTAO="hpc-performance memory-access"
VTRO="-data-limit=0 -finalization-mode=none -no-summary -trace-mpi -result-dir ./oVTP:all"

# ============================ NPB ============================================
C=0
for BEST in ${BESTCONF} ${SCALCONF}; do
	C=$((C+1))
	for BMconf in ${BBINARYS}; do
		BINARY="$(echo ${BMconf} | cut -d '|' -f-2 | tr '|' '.').x"
		INPUT="$(echo ${BMconf} | cut -d '|' -f5)"
		if [ $C -eq 1 ]; then
			#BESTCONF branch
			NumMPI="$(echo ${BMconf} | cut -d '|' -f3)"; if skip_conf "${NumMPI}"; then continue; fi
			NumOMP="$(echo ${BMconf} | cut -d '|' -f4)"
		else
			#SCALCONF branch
			NumMPI="$(echo ${BEST} | cut -d '|' -f1)"; if skip_conf "${NumMPI}"; then continue; fi
			NumOMP="$(echo ${BEST} | cut -d '|' -f2)"
		fi
		if   [ "x${NumMPI}"  = "x1" ] && [[ "${BINARY}" = *"-MPI/"* ]]; then continue;
		elif [ "x${NumMPI}" != "x1" ] && [[ "${BINARY}" = *"-OMP/"* ]]; then continue; fi
		BName="$(basename ${BINARY})"
		TenRanks="$(python3 -c "from random import sample, seed; seed(0); need=10; print(0, ' '.join(['%s' % x for x in sorted(sample(range(1, $NumMPI), k=need-1))])) if $NumMPI > need else print(' '.join(['%s' % y for y in range($NumMPI)]))")"
		LOG="${LOGDIR}/${BName}.log"
		if [ "x$RUNSDE" = "xyes" ]; then
			echo "=== sde run ===" >> $LOG 2>&1
			echo "$(get_mpi_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "") bash -c \"${SDE} ${BINARY} ${INPUT}\"" >> ${LOG} 2>&1
			for R in $TenRanks; do
				$(get_mpi_cmd "${NumMPI}" "${NumOMP}" "${LOG}" "") bash -c "if [[ \$PMIX_RANK = ${R} ]]; then ${SDE} ${BINARY} ${INPUT}; else ${BINARY} ${INPUT}; fi" >> ${LOG} 2>&1
				clenup_after_mpi_cmd
			done
			mkdir -p ${LOG}_${NumMPI}_${NumOMP}_sde; mv dcfg-out.* ${LOG}_${NumMPI}_${NumOMP}_sde/
			rm -f ADC.*
		fi
		if [ "x$RUNPCM" = "xyes" ]; then
			sleep 0
		fi
		if [ "x$RUNVTUNE" = "xyes" ]; then
			sleep 0
		fi
	done
done
cd ${ROOTDIR}

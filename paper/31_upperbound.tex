\subsection{Machine Code Analyzers Assuming Unrestricted Locality}\label{ssec:L1sim}


Before launching multi-months executions of cycle-accurate simulations, one would want to have a first-order
approximation of a very large and fast cache. We design a simulation approach, using Machine Code Analyzers
(MCA), which can estimate the speedup for a given application orders-of-magnitude faster than gem5 (cf.~next
section). This upper bound in expected performance improvement allows us to:
\textbf{(i)}~get a perspective on the best possible performance improvement if all read/writes can be satisfied from the cache; and
\textbf{(ii)}~justify more accurate simulations and classify their results with respect to the baseline and the upper bound.

Machine Code Analyzers, such as llvm-mca~\cite{llvm_project_llvm-mca_2022}, have been designed to study
microarchitectures, improve compilers, and investigate resource pressure for application kernels. Usually,
the input for these tools is a short Assembly sequence and they output, among other things, an expected
throughput for a given CPU when the sequence is executed many times and all data is available in L1 data
cache. For most real applications the latter assumption is obviously incorrect, however it is ideal to
gauge an upper bound when all memory-bottlenecks disappear.

Unfortunately, it is neither feasible to record all executed instructions in one long sequence, nor to analyze
a full program sequence with llvm-mca. Hence, we break the program execution into basic blocks (at most tens or
hundreds of instructions) and evaluate their throughput individually. For a given combination of a program
and input (called \textit{workload} hereafter), the basic blocks and their dependencies create a directed Control Flow
Graph (CFG)~\cite{intel_corporation_dynamic_2020} with one source (program start) and one sink (program
termination). All intermediate nodes (representing basic blocks) of the graph can have multiple parent- and
dependent-nodes, as well as self-references (e.g. basic blocks of \texttt{for}-loops). Knowing the ``runtime''
of a each basic block and the number of invocations per basic block, we can estimate the runtime of the entire
workload by summation of the parts.

We utilize Intel's Software Development Emulator (SDE)~\cite{intel_corporation_intel_2021} to record the basic
blocks and their caller/callee dependencies for a workload with modest runtime overhead (typically in order of
1000x slowdown).
SDE also notes down the number of invocations per CFG edge for a workload, i.e., how often the program counter (PC)
jumped from one specific basic block to another specific block. We developed a program which parses the
output of Intel SDE and establishes an internal representation of the Control Flow Graph. The internal
CFG nodes are then amended with Assembly extracted from the program's binary, since SDE's Assembly output is
not compatible with Machine Code Analyzers. Our program subsequently executes a Machine Code Analyzer for
each basic block, getting in return an estimated cycles-per-iteration metric (CPI). We record the per-block CPI
at the directed CFG edge from caller to callee, which already holds the number of invocations of this edge,
effectively creating a ``weighted'' graph. Figure~\ref{fig:mcatool} showcases the result and it is
easy to see that the summation of all edges in the CFG is equivalent to the estimated runtime of the entire workload (assuming
all data is inside the L1 data cache).


The above outlined approach works  not only for sequential programs, but also for parallel applications.
Intel SDE can record the instruction execution and caller/callee dependencies for thread-parallel
programs, e.g. pThreads, OpenMP, or TBB. Furthermore, we can attach SDE to individual MPI ranks to get the
data for it. Therefore, we are able to estimate the runtime for MPI+X parallelized HPC applications by the
following equation:
\begin{equation}\textstyle
    \text{t}_\text{app} := \frac{ \max\limits_{\forall r\,\in\,\text{ranks}} \big( \max\limits_{\forall t\,\in\,\text{threads}_{r}} (\,\sum\limits_{\forall \text{edges}\,e\,\in\,\text{CFG}_{t,r}} \text{CPI}_{e} \cdot \#\text{calls}_{e}\,)\big) }{ \text{processor frequency in Hz} }
    \label{eq:mca}
\end{equation}
under the assumption that MPI ranks and threads do not share computational resources\footnote{Resource over-subscription is outside the scope of this study and our tool.}.

The self-imposed restriction of Machine Code Analyzers is the limited accuracy compared to cycle-accurate
simulators, due to their distinct design goal. To improve our CPI estimate, we rely on four different MCAs,
namely llvm-mca~\cite{llvm_project_llvm-mca_2022}, Intel ACA (IACA)~\cite{intel_corporation_intel_2012},
uiCA~\cite{abel_parametric_2021}, and OSACA~\cite{laukemann_automated_2018}, and take the median of the results.
Another shortcoming of MCA tools is that most of them estimate the throughput of basic blocks in isolation while
assuming looping behavior of the assembly block (PC jumps from last back to first instruction).
Neither ``block looping'' nor an empty instruction pipeline (single iteration of the block) are realistic for
some blocks.
Hence, for non-looping basic blocks, we estimate the CPI by feeding the MCA tool with the blocks of caller and
callee, and the callee's CPI is calculated by subtracting the cycle of retirement of its last instruction from
the caller's last instruction retirement (instead of when the callee's first instructions are decoded, which
can overlap with execution of caller instructions). Further, we correct some cycle estimates for specific
instructions within our tool in a post-processing step, since we encountered a few unsupported or grossly
mis-estimated instructions while validating our tool against known benchmarks. We refer the reader to Section~\ref{ssec:L1simVali}
for more details.

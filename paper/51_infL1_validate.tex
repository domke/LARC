\subsection{MCA-based Simulator Validation}\label{ssec:L1simVali}

\begin{figure}[tbp]
    \centering
    \includegraphics[width=\linewidth]{figures/l1simvali}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-4ex}}
    \caption{Validation of MCA-based performance estimation against PolyBench/C \texttt{MINI} with inputs fitting into L1D; Relative runtime shown (vs.~Intel E5-2650v4 measurements); Values $\geq\,$1 show prediction of faster execution}
    \label{fig:l1simvali}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
\end{figure}

During the development of our MCA-based simulator, we implemented numerous micro-benchmarks to fine-tune the
CPI estimation capabilities while comparing the results to an \mbox{Intel} Broadwell (\mbox{E5-2650V4}). Our
micro-benchmarks comprise MPI-/OpenMP-only, MPI+OpenMP, and single-threaded tests (exercising
recursive functions, floating-point- or integer-intensive operations, L1-localised, or stream-like operation).

Needless to say, applying MCA-based simulations to full workloads or complex application kernels is still
error-prone, since these tools are designed to analyze small Assembly sequences without guarantee for
accurate absolute performance numbers. Regardless, we validate the current status of our tool
using PolyBench/C with \texttt{MINI} inputs. In theory, these input sizes ($\approx\,$\unit[16]{KiB}) should
all fit into the \unit[32]{KiB} L1D cache of the Broadwell. Hence, measuring the kernel execution time for these
PolyBench tests should yield numbers close to MCA-based runtime estimates. For the baseline measurements,
we set all cores of the Broadwell to \unit[2.2]{GHz}, set the uncore to~\unit[2.7]{GHz}, and disable turbo boost; compile each workload with Intel's
Parallel Studio XE\footnote{For details of flags, tools, versions, and exec.
env, please refer to AD/AE.}, and execute every test for 100 times (since
many only run for a few \unit[]{ms}) to determine the fastest possible execution time.
The difference between the real baseline results and our MCA-based estimates is visualized in
Figure~\ref{fig:l1simvali} as projected relative runtime difference.

The data shows that on average our MCA-based method slightly overestimates a faster execution. Only seven
out of 30 workloads are expected to run slower than what we observe on the real Broadwell (i.e., y-value $\leq$1).
For eight of the PolyBench tests, our tool estimates the runtime to be over 2x faster than our measurements.
Hence, we can conclude that for 73\% of the micro-benchmarks the MCA-based method is reasonably accurate: within
2x slower-to-2x faster. While a 2x discrepancy might appear high, we have to point out that our
cross-validations using SST~\cite{rodrigues_improvements_2012,voskuilen_analyzing_2016} and third-party
gem5 models~\cite{akram_validation_2019} for Intel CPUs yield similar inaccuracies\footnote{A large-scale
survey of academic simulators in realistic scenarios, beyond carefully selected and tuned micro-kernels, is---in our humble opinion---consequential yet outside the scope of this paper.}, but our MCA-based method is substantially faster.

\begin{figure*}[tbp]
%    \centering
    \includegraphics[width=\linewidth]{figures/l1simresults_top}
    \hfil
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
    \includegraphics[width=\linewidth]{figures/l1simresults_bottom}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-1ex}}
    \caption{Projected speedup, assuming all data fits into L1D with ``optimistic'' load-to-use latency; Top row, left to right: PolyBench, RIKEN TAPP kernels, NPB (OMP); Bottom row, left to right: NPB (MPI), TOP500 etc., ECP proxies, RIKEN Fiber apps, SPEC CPU[int/single] and CPU[float/OMP], SPEC OMP; Missing bar for wrf: one unknown instruction (\texttt{rex.W}) causes llvm-mca errors [we will update the figure for the camera-ready version]}
    \label{fig:l1simresults}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
\end{figure*}

Another indicator for the accuracy of our MCA-approach can be drawn from DGEMM.
Theoretically, DGEMM
performs close to peak and is not memory-bound for large matrices, and hence the measured runtime and MCA-based
estimates are expected to match. Unfortunately, PolyBench's \unit[]{Gflop/s} rate for \texttt{gemm} is far
from peak (due to its hand-coded loop-nest), and therefore we replace it with an Intel MKL-based implementation
of equal matrix dimensions. For the PolyBench input sizes $\texttt{MINI},\ldots,\texttt{EXTRALARGE}$ in our MKL-based
implementation, our MCA tool estimates a faster runtime by 6.4x, 75\%, 11\%, 1.9\%, and 1.5\%, respectively.
This closely matches the achievable single-core \unit[]{Gflop/s} of the \mbox{E5-2650V4}: for \texttt{MINI}
and the MKL-based runs, we measure only \unit[2]{Gflop/s}, while for \texttt{EXTRALARGE} we peak out at the expected
\unit[32]{Gflop/s}. The low \unit[]{Gflop/s} measurements for \texttt{MINI} (and \texttt{SMALL}) demonstrate
that MKL is not yet compute-bound, and hence causes the 6.4x (and 75\%) misprediction.


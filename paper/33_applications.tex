\subsection{Relevant HPC (Proxy-)Applications and Benchmarks}\label{ssec:apps}
Instead of relying on a narrow set of cherry-picked applications, we attempt to cover a broad spectrum of typical
scientific workloads executed on modern supercomputers. We customize and extend a publicly
available benchmarking framework~\cite{domke_matrix_2021,domke_matrix_2021-1} with a few additional benchmarks
and necessary features to perform the MCA- and gem5-based simulations. Hereafter, we detail the list of
127 included workloads, summed up across all benchmark suites, which are sized to fit within a single node and which could be simulated with gem5 in a
reasonable time ($\leq\,$six months).

\subsubsection{Polyhedral Benchmark Suite}
The PolyBench/C suite contains 30 single-threaded, scientific kernels which can be parameterized in
memory occupancy ($\in[\text{\unit[16]{KiB}},\text{\unit[120]{MiB}}]$)~\cite{pouchet_polybenchc_2016}. Unless stated otherwise,
we use the largest configuration.

\subsubsection{TOP500, STREAM, and Deep Learning Benchmarks}
High Performance Linpack (HPL)~\cite{dongarra_linpack_1988} solves a dense system of linear equations $Ax = b$ of size
36,864 in our case. High Performance Conjugate Gradients (HPCG)~\cite{dongarra_hpcg_2015}  applies  a conjugate gradient
solver to a system of linear equation (represented by a sparse matrix $A$). We choose $120^3$ for
HPCG's global problem size. BabelStream~\cite{deakin_gpu-stream_2016} evaluates the memory subsystem of CPUs and
accelerators, and we configure~\unit[2]{GiB} input vectors. Additionally, we implement a micro-benchmark to approximate
the single-precision GEMM operation ($m=\text{1577088}; n=\text{27}; k=\text{32}$) which is commonly found in 2D deep
convolutional neural networks (CNN), such as 224$\times$224 ImageNet classification workloads~\cite{vasudevan_parallel_2017}.

\subsubsection{NASA Advanced Supercomputing Parallel Benchmarks}
The NAS Parallel Benchmarks (NPB)~\cite{bailey_nas_1991,van_der_wijngaart_nas_2002} consists of nine kernels and proxy-apps
which are common in computational fluid dynamics (CFD). The original MPI-only set has been expanded with ten OpenMP-only
benchmarks~\cite{jin_openmp_1999} and we select the \texttt{class B} input size for all of them.


\subsubsection{RIKEN's Fiber Mini-Apps and TAPP Kernels}
%\todo{B: if we need more space we could perhaps compress these app descriptions?}
To aid the co-design of Supercomputer Fugaku, RIKEN developed the Fiber proxy-application set~\cite{riken_aics_fiber_2015},
a benchmark collection representing the scientific priority areas of Japan. Additionally, RIKEN released
numerous scaled-down TAPP kernels~\cite{riken_center_for_computational_science_kernel_2021} of their priority applications
which are tailored for fast simulations with gem5~\cite{kodama_accuracy_2020}. 
The proxy-apps and workloads we use are as follows.
\emph{FFB}~\cite{guo_basic_2006} with the 3D-flow problem discretized into 50$\times$50$\times$50 sub-regions; 
\emph{FFVC}~\cite{ono_ffv-c_nodate} using 144$\times$144$\times$144 cuboids;
\emph{MODYLAS}~\cite{andoh_modylas:_2013} with the \texttt{wat222} workload;
\emph{mVMC}~\cite{misawa_mvmc--open-source_2018} with the strong-scaling test reduced to 1/8th of the samples and 1/3rd of the lattice size;
\emph{NICAM}~\cite{tomita_new_2004} with a single (not 11) simulated day;
\emph{NTChem}~\cite{nakajima_ntchem:_2014} with the H\textsubscript{2}O workload;
\emph{QCD}~\cite{boku_multi-block/multi-core_2012} with the \texttt{class 2} input.

\subsubsection{Exascale Computing Project Proxy-Applications}
Similar to RIKEN, the US-based supercomputing centers curated a co-design benchmarking suite
for their recent exascale efforts~\cite{exascale_computing_project_ecp_2018}. 
We select eleven applications of the aforementioned benchmarking framework with the following workloads.
\emph{AMG}~\cite{park_high-performance_2015} with the \texttt{problem~1} workload;
\emph{CoMD}~\cite{mohd-yusof_co-design_2013} with the 256,000-atom strong-scaling test;
\emph{Laghos}~\cite{dobrev_high-order_2012} modelling a 3D Sedov blast but with 1/6th of the timesteps;
\emph{MACSio}~\cite{dickson_replicating_2016} with an $\approx\,$\unit[1.14]{GiB} data dump distributed across many \texttt{JSON} files;
\emph{MiniAMR}~\cite{heroux_improving_2009} simulating a sphere moving diagonally through 3D space;
\emph{MiniFE}~\cite{heroux_improving_2009} with 128$\times$128$\times$128 grid size;
\emph{MiniTri}~\cite{wolf_task-based_2015} testing triangle- and largest clique-detection on \texttt{BCSSTK30} (MatrixMarket~\cite{boisvert_matrix_1997});
\emph{Nekbone}~\cite{argonne_national_laboratory_nek5000_nodate} with 8,640 elements and polynomial order of~8;
\emph{SW4lite}~\cite{petersson_users_2017} simulating a \texttt{pointsource};
\emph{SWFFT}~\cite{habib_hacc:_2016} with 32 forward and backward tests for a 128$\times$128$\times$128
grid;
\emph{XSBench}~\cite{tramm_xsbench_2014} with the \texttt{small} problem and 15 million particle lookups.



\subsubsection{SPEC CPU and SPEC OMP Benchmarks}
The Standard Performance Evaluation Corporation~\cite{standard_performance_evaluation_corporation_specs_2020} offers,
among other benchmarks, two HPC-focused test suits: SPEC CPU[speed] (ten integer-heavy, single-threaded; ten
OpenMP-parallelized, floating-point benchmarks) and SPEC OMP (14 OpenMP-parallelized benchmarks). All SPEC tests hereafter
are based on non-compliant runs with the \texttt{train} input configuration.
%falls under fair-use if we don't use official metric names and state non-compliance,
%see https://www.spec.org/fairuse.html#ComplianceExceptions for details

\subsection{Speedup-potential with Restricted Locality}\label{ssec:gem5Results}

To further refine our projections gained by abundant L2  cache, we proceed with the
cycle-level simulations of the proxy-applications and benchmarks listed in Section~\ref{ssec:apps}.

We compile all benchmarks with Fujitsu's Software
Technical Computing Suite (v4.6.1) targeting the real A64FX, and simulate the single-rank workloads in gem5 for
our four versions.
Unfortunately, three of our MPI-based benchmarks require multi-rank MPI: \mbox{MODYLAS} (requires $\geq\text{8}$ ranks),
NICAM ($\geq\text{10}$ ranks), and NTChem (complex message pattern with rank 0 sending/receiving from itself), and hence
we omit them. Furthermore, we skip the MPI-only versions of NPB.

The per-configuration speedup is given relative to the baseline A64FX\TTT{S} configuration.
We exclude initialization and post-processing times, and measure only the main kernel runtime, except for the SPEC benchmarks as described in Section~\ref{ssec:L1simResults}.
These results are presented in Figure~\ref{fig:gem5results} and show the effects of the gradual expansion of simulated resources.
The average (single CMG) speedups from \procC{} and \procA{} are $\approx\,$1.9x and $\approx\,$2.1x, respectively, with some applications reaching $\approx\,$4.4x for \procC{} and $\approx\,$4.6x for \procA{}.

As expected, most benchmarks benefit from the additional cores and increased cache size, most prominently MG-OMP which gains a small speedup of $\approx\,$1.3x from the extra cores, $\approx\,$2x speedup from the extra cache, and with~\unit[512]{MiB} cache and higher bandwidth reaches a speedup of $\approx\,$4.6x.
Comparable incremental improvements with all three architecture steps are observable in other workloads, such as TAPP Kernel 7 and 17, showing good scaling on multiple cores and being memory-bound since they benefit from both the higher core count and the cache size upgrade.
Kernel 19 and 20, XSBench, roms, and \mbox{imagick~(SPEC OMP)} show similar gain in runtime, but the difference between \procC{} and \procA{} is significantly smaller, implying that the problem size either fits into the~\unit[256]{MiB}~L2 (e.g., XSBench) or the workload arrives at a point of diminishing returns from the~2x larger cache.
Kernel 8, 9, 12--15, and FT-OMP suffer a slowdown from cache contention in A64FX\TSS{32}.
\procC{} and \procA{} mitigate the cache contention resulting in speedups similar to the benchmarks discussed earlier.
EP-OMP, CoMD, and other compute-bound benchmarks benefit only from the higher core count, with both \proc{}s providing  similar speedup as A64FX\TSS{32}.

\begin{table}[tbp]
    \caption{L2 cache-miss rate [in \%] of representative proxy applications}
    \label{tbl:gem5missrate}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-1ex}}
    \centering
    \begin{tabularx}{\columnwidth}{lRRRR}
        \toprule
        \tH{Proxy-App}  & \tR{A64FX\TTT{S}} & \tR{A64FX\TSS{32}}    & \tR{\procC{}} & \tR{\procA{}} \\
        \midrule
        Kernel 12	& 36.6	& 47.6	& 10.5	& 9.1   \\
        Kernel 17   & 46.7  & 49.5  & 48.7  & 34.8  \\
        Kernel 19	& 73.8	& 69.6	& 49.1	& 48.9  \\\addlinespace[.2em]
        FT-OMP	    & 11.6	& 48.2	&  6.4	& 3.8   \\
        MG-OMP      & 59.8	& 70.9	& 29.4	& 0.4   \\\addlinespace[.2em]
        XSBench	    & 32.1	& 36.4	&  0.1	& 0.1   \\
        \bottomrule
    \end{tabularx}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
\end{table}

Expectedly, single-threaded workloads (all of PolyBench's benchmarks) show little to no improvements over A64FX\TTT{S}, i.e.,\ they do not benefit from more cores.
However, these
benchmarks also do no show a performance gain from a larger 3D-stacked L2 cache, albeit their working set size exceeding A64FX\TTT{S}' \unit[8]{MiB} L2 but fitting into \proc{}' larger cache. We only see a limited speedup of ($GM$=)4.3\% across
all of them and no noteworthy outliers, and hence omit them in Figure~\ref{fig:gem5results}.
We attribute other outliers, such as the slowdown of imagick (SPEC-CPU), to similar intrinsic property of the benchmark:
our testing on a real A64FX reveals that imagick has a sweetspot at 8 OpenMP threads, and scales negatively thereafter; and
the TAPP Kernels 3--6 and 18 were customized for the 12-core A64FX CMG and cannot run effectively on 32 threads without a rewrite.
Hence, we limit gem5 to 12 cores for these TAPP kernels, and we see that only Kernel~18 benefits from a larger L2.
Further proxy-applications and benchmarks missing from Figure~\ref{fig:gem5results}, yet appearing in Figure~\ref{fig:l1simresults},
are the unfavorable result of persistent, repeatable simulator errors---sometimes occurring after months of simulation.

We should note that in some cases the benchmarks' implementation and the quality of the compiler may skew the results,
for instance, BabelStream measuring memory bandwidth on a~\unit[2]{GiB} buffer.
Being unoptimized for A64FX, BabelStream's baseline underperforms in terms of per-core BW (compared to STREAM Triad tests in Figure~\ref{fig:gem5valid-cores} and~\ref{fig:gem5valid-size}) which in turn results in performance gain when the number of cores increases
to~32.

Overall, the speedup on A64FX\TSS{32} can originate from the following reasons:
\textbf{(i)}~the program is compute-bound (a valid result);
\textbf{(ii)}~the workload exhibits both compute-bound and memory-bound tendencies in different components of a proxy-application (a valid result);
\textbf{(iii)}~the program is highly latency-bound, and hence the speedup can be the result of the larger aggregate L1 cache (a valid result); or
\textbf{(iv)}~a poor baseline resulting in a slightly misleading result.


We confirm the validity of attributing improvement to the high capacity L2 by inspecting the L2 cache-miss rates of our gem5 simulations (miss rate of some selected examples listed in Table~\ref{tbl:gem5missrate}). The reduction in cache-miss rates reported in the table is consistent with  the performance improvements we observe in Figure~\ref{fig:gem5results}.

%%% IMPORTANT, DON'T WRITE PAST THIS
%%% Local Variables:
%%% TeX-master: "01_main"
%%% End:

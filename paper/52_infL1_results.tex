\subsection{Speedup-potential with Unrestricted Locality}\label{ssec:L1simResults}

Knowing the limits of the MCA-based approach, we can take on the entire benchmark suite from
Section~\ref{ssec:apps} and evaluate their speedup potential when all data fits into L1.

The baseline measurements are conducted similar to Section~\ref{ssec:L1simVali} on a dual-socket
Intel E5-2650v4 system with 48 cores (2-way hyper-threading enabled).
For all listed benchmarks,
excluding SPEC CPU and OMP, we focus on the solver times only, i.e., we ignore data initialization and
post-processing phases. Since most of the proxy-apps are parallelized with MPI and/or OpenMP, we
perform an initial sweep of possible configurations of ranks and threads to determine the fastest
time-to-solution (TTS) for our strong-scaling benchmarks, and the highest figure-of-merit (as reported by the
benchmarks) for weak-scaling benchmarks. The highest performing configuration is executed ten times to determine
the TTS of the kernel as our reference point in Figure~\ref{fig:l1simresults}.

The same MPI/OMP configurations are then used for our MCA-based estimate. Under the assumption
that some MPI-parallized benchmarks experience imbalances, we randomly sample up to nine ranks
(in addition to rank~0)\footnote{Sampling at most ten out of all MPI ranks should not substantially alter
the result but saves resources, since we have to execute SDE once per rank.}, execute the selected rank with
Intel SDE (and the remaining ranks normally), and calculate the estimated runtime using Equation~\ref{eq:mca}
and the~\unit[2.2]{GHz} processor frequency. The resulting runtime is divided by the measured runtime to
determine the upper-bound speedup potential per application when all its data would fit into L1D,
see Figure~\ref{fig:l1simresults}.

For PolyBench/C workloads, we see similar speedup trends as for its smallest inputs which we used
in Figure~\ref{fig:l1simvali}, although the expected speedup for \texttt{EXTRALARGE} increases
to a peak of~8.4x for the ludcomp kernel. Only four kernels show no performance increase, presumably
by being compute-bound and not bandwidth-bound: 2mm, 3mm, doitgen, and trisolv. Overall, the MCA-based
approach estimates a geometric mean~($GM$) performance increase of 2.9x from fitting all data into L1D.
%
RIKEN's TAPP kernels benefit the most from unrestricted locality. Especially Kernel~20, which represents
one core function of the FFB application, shows a speedup of~20x. Altogether, we see a projection of
($GM$=)2.6x increased performance, but also two cases (Kernel 5 and 9) where the MCA tool estimates
an $\approx\,$50\% slowdown. These two are from GENESIS~\cite{jung_genesis_2015} and NICAM,
respectively, but as detailed in Section~\ref{ssec:L1simVali}, some inaccuracy is expected as the
trade-off for the faster simulation time.

NPB's OpenMP version of a conjugate gradient (CG) solver is another workload with large theoretical performance gain of 13.1x.
In total, we expect a ($GM$=)3x gain for all NAS Parallel Benchmarks; specifically, ($GM$=)4x for the OpenMP versions and ($GM$=)2.3x for the MPI versions, respectively. The potential gain
for CG is not surprising, since these solvers are predominantly bound by memory bandwidth and are sensitive to memory latency~\cite{dongarra_new_2016}.
%
High Performance Linpack is unsurprisingly not expected to gain any performance by placing all
its data into L1 cache, as this benchmarks is compute-bound. In fact, our MCA tool expected
a small runtime decrease of 11\%. By contrast, DLproxy, which uses MKL's SGEMM, would benefit
from a large L1, since MKL cannot achieve peak~\unit[]{Gflop/s} for the tall/skinny matrix in this
workload (cf.~Section~\ref{ssec:apps}).
%
XSBench and miniAMR show the highest gains for ECP's and RIKEN's proxy-apps, with a value of 7.3x and 7.4x,
respectively. This appears to be in line with expectation from the roofline characteristics of the benchmarks
when measured on a similar compute node~\cite{domke_double-precision_2019}.

A deeper look at roofline analysis in~\cite{domke_double-precision_2019} reveal that there is no strong correlation
between the position of an application on the roofline model and the expected performance gain from solely running out of L1D cache.
We speculate that other, hidden bottlenecks are exposed by our MCA approach, such as data
dependencies and lack of concurrency in the applications, which limit the expected speedup.
%
Apart from noticeable outliers in the expected speedup, such as lbm, bwaves, botsspar, and ilbdc,
the potential from enlarged L1D is rather slim for SPEC, and only ($GM$=)1.9x runtime reduction
can be expected across all 33\fixme{when wrf done} workloads. Our simulation with gem5, in the following
sections will reveal if those speedups can also be obtained by sufficiently large, 3D-stacked
L2 cache, or if we fall short on our expectations.


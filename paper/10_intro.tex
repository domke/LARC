\section{Introduction}\label{sec:intro} %\fixme{~~page 1}

Historically, the reliable performance increase of von Neumann-based general-purpose processors (CPUs)
was driven by two technological trends. The first, observed by Gordon E.~Moore~\cite{moore_progress_1975},
is that the number of transistors in an integrated circuit doubles roughly every two years.
The second, called Dennard's scaling~\cite{dennard_design_1974}, postulates that as transistors get
smaller their power density stays constant. These trends synergized well, allowing computer
architectures to continuously improve performance through, for example, aggressive pipelining
and superscalar techniques without running into thermal limitations by, e.g., reducing the
operating voltage. In the early 2000s, Dennard's scaling ended~\cite{hruska_death_2012} and
forced architects to shift their attention from improving instruction-level parallelism
to exploiting on-chip multiple-instruction multiple-data
parallelism~\cite{gottlieb_nyu_1983}. This immediate remedy to the end of Dennard's scaling applies
to this day in the form of processors such as Fujitsu A64FX~\cite{sato_co-design_2020},
AMD Ryzen~\cite{suggs_amd_2020}, or NVIDIA GPUs~\cite{owens_gpu_2008,nickolls_gpu_2010}.

Unfortunately, Moore's law is impending termination~\cite{theis_end_2017}, and we are entering
a post-Moore era~\cite{vetter_architectures_2017}, home to a diversity of architectures,
such as \mbox{quantum-,} \mbox{neuromorphic-,} or reconfigurable computing~\cite{hemsoth_rogues_2018}.
Many of these prototypes hold promise but are still immature, focus on a niche use case,
or incur long development cycles. At the same time, there is one salient post-Moore architecture
that is growing in maturity and which can facilitate performance improvements in the decades to
come even for the classic von Neumann CPUs we have come to rely upon---3D integrated circuit
(IC) stacking~\cite{black_stacking_2006}.

\begin{figure}[tbp]
    \centering
    \includegraphics[width=\linewidth]{figures/minife_10_increments}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
    \caption{MiniFE: relative performance improvement of AMD EPYC 7773X \mbox{Milan-X} over AMD EPYC 7763 Milan, and Figure of Metrit; Input problem scaled from 100$\times$100$\times$100 to 400$\times$400$\times$400; Both systems equipped with dual-socket CPUs; Benchmark run with 16 MPI ranks and 8 OpenMP threads} %add numbers in fig: AMD EPYC 7763 Milan and 7773X \mbox{Milan-X}
    \label{fig:milan-minife}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
\end{figure}

3D integrated circuits refer to the general technologies of vertically building integrated circuits
and can be done in multiple ways, such as by stacking multiple discrete dies and connecting them
using coarse through-silicon vias (TSVs) or growing the 3D integrated circuit monolithically on the
wafer~\cite{shulaker_monolithic_2015}. 3D integrated circuits have multiple benefits~\cite{hu_stacking_2018},
including
\textbf{(i)}~shorter wire lengths in the interconnect leading to reduction in power consumption, 
\textbf{(ii)}~improved memory bandwidth through on-chip integration that can alleviate performance
bottlenecks in memory-bound applications,
\textbf{(iii)}~higher package density, which yields more compute and smaller system footprint, and
\textbf{(iv)}~possibly lower fabrication cost due to smaller die size (thus improved yield). 
All these are very desirable benefits in today's exascale (and future) High-Performance Computing (HPC)
systems. Recent advances in 3D integrated circuits have enabled many times higher capacity on-chip
memory (caches) than traditional systems (e.g., AMD V-Cache~\cite{evers_amd_2022}) in the hope that
the larger cache sizes will materialize---as conventional wisdom and maxims tells us---in higher
performance. But how far can 3D ICs (with a focus on increased on-chip cache) take us in HPC?

Intuition tells us that an increased cache size will help alleviate the performance bottlenecks
of many key HPC applications. To demonstrate this, we conduct a pilot study where we execute one
of the important proxy-apps from the DoE ExaScale Computing Project (ECP) suite,
MiniFE~\cite{heroux_improving_2009} (see~Section~\ref{ssec:apps} for details), on AMD EPYC Milan and
\mbox{Milan-X} CPUs---two architecturally similar processors with vastly different L3 cache
sizes~\cite{bonshor_amd_2022}. Figure~\ref{fig:milan-minife} overviews our result, and we see that
for a subset of problem sizes, the 3-times larger L3 capacity of \mbox{Milan-X} yields up-to~3.4x
improvements over baseline Milan for this memory-bound application, which motivates us to further
research 3D-stacked caches.

\textbf{Contributions:} In this study, we seek to unveil the impact that future large caches could have on the next generation of HPC systems.
We study our research questions from three different levels of abstraction: \textbf{(i)}~we design a novel exploration framework that allows us
to simulate HPC applications running on a hypothetical processor having infinitely large L1D cache. We use this framework, that is orders of magnitude faster than cycle-accurate simulators, to estimate an upper-bound
for cache-based improvements;
\textbf{(ii)}~we model a future processor, \emph{\proc{}} (LARge Cache processor), that builds on the design of A64FX, with eight stacked SRAM dies
under \unit[1.5]{nm} manufacturing assumption; \textbf{(iii)}~we complement our study with a plethora of simulations for numerous
CPU micro-benchmarks and common HPC proxy-applications; and
\textbf{(iv)}~we find that half (29 out of~52) of the simulated applications experience a $\geq\,$2x speedup on LARC's Core Memory Group (CMG) compared to our A64FX CMG baseline at one-fourth of the area.
For applications that are responsive to larger cache capacity, this would translate to an average improvement of 9.77x (geometric mean) when we assume ideal scaling and compare at the full chip level.


set terminal svg size 1000,210 dynamic enhanced fname "Times" butt dashlength 1.0
set output "../figures/gem5results.svg"

set style data histogram
set style histogram cluster gap 1
set style fill solid 1.00 border lt -1
set key left top opaque vertical Left reverse enhanced autotitles columnhead box width +.5 font ",9"
set grid nopolar noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics y2tics nomy2tics nocbtics nomcbtics

set auto x
set xrange [-.5:51.5]
set xtic font ",11" rotate by -45 scale 0 left offset -.5,0

set auto y
set ylabel "Speedup" offset 1,0 font ",12"
#set ytics border nomirror norotate offset character 0, 0, 0 autojustify font ",12"
set yrange [0:5]
set ytics 1
set ylabel "Speedup"

min=5
fixPeak(x) = (x>min) ? min : x

set label "8.8x" at  6,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "8.7x" at 11,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "5.7x" at 12,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "6.6x" at 16,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "5.6x" at 17,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "7.2x" at 18,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "20x"  at 19,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "13x"  at 22,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "7.0x" at 24,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "5.1x" at 25,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "5.9x" at 26,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "7.3x" at 28,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "7.3x" at 34,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"
set label "5.3x" at 46,5.3 left rotate by 45 tc rgb "#3b7696" font ",9"

set arrow 1 from graph 0, first 1 to graph 1, first 1 nohead front lc rgb 'black' dt 2 lw 1.5

set label "RIKEN TAPP" at 8,4.5 left tc rgb "#797A79" font ",8"
set arrow 2 from -.2,4.7 to 21.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "NPB (OMP)" at 23,4.5 left tc rgb "#797A79" font ",8"
set arrow 3 from 21.8,4.7 to 27.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
#set label "TOP500, etc." at 29,4.5 left tc rgb "#797A79" font ",8"
#set arrow 4 from 27.8,4.7 to 29.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "ECP" at 31,4.5 left tc rgb "#797A79" font ",8"
set arrow 5 from 29.8,4.7 to 34.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "SPEC CPU" at 38,4.5 left tc rgb "#797A79" font ",8"
set arrow 6 from 34.8,4.7 to 43.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "SPEC OMP" at 46.5,4.5 left tc rgb "#797A79" font ",8"
set arrow 7 from 43.8,4.7 to 51.2,4.7 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5

plot "../data/gem5results.data" u 2 fs pattern 2 lw .5 lc rgb "#d95f02" title "AFX64^{32}", \
     "" u 3                                     lw .5 lc rgb "#1b9e77" title "LARC_C", \
     "" u 4:xtic(1)                             lw .5 lc rgb "#7570b3" title "LARC^A", \
     "" u 5 w points lt 12 pointsize 0.1  lc rgb "#56b4e9" notitle, \
     "" u 5 w points lt 12 pointsize 0.1  lc rgb "#56b4e9" notitle, \
     "" u 5 w points lt 12 pointsize 0.1  lc rgb "#56b4e9" notitle, \
     "" u 5 w points lt 12 pointsize 0.1  lc rgb "#56b4e9" notitle, \
     "" u 5 w points lt 12 pointsize 0.1  lc rgb "#56b4e9" notitle, \
     "" u (fixPeak($5)) w points lt 12 pointsize .5 lc rgb "#56b4e9" title "MCA", \

set terminal svg size 1000,450 dynamic enhanced fname 'Times' butt dashlength 1.0
set output "../figures/minife_10_increments.svg"

set boxwidth 0.75 absolute
set style fill transparent solid .4 border lt -1
set style histogram rowstacked title  offset character 0, 0, 0
set style data histograms

set border
set grid nopolar
set grid noxtics nomxtics noytics nomytics noztics nomztics nox2tics nomx2tics y2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

set key right top opaque vertical Left reverse enhanced autotitles columnhead box width -12 font ",16"

set auto x
set xtics border in scale 1,0.5 nomirror norotate offset character 0, 0, 0 autojustify font ",24" offset 0,.3
#set xrange [95:405]
set xtics 10 rotate by -45 left offset -.5,0 font ",22"

set auto y
set ytics border nomirror norotate offset character 0, 0, 0 autojustify font ",22"
set yrange [0:3.5]
set ytics 1

set auto y2
set y2tics border nomirror norotate offset character 0, 0, 0 autojustify font ",22"
set y2range [0:]
set y2tics 100

set xlabel "Input cube dimension (nx = ny = nz)" offset 0,-.4 font ",22"
set ylabel "Speedup" offset 0,1 font ",22"
set y2label "Figure-of-merit [in Gflop/s]" offset 0,1 font ",22"

set arrow 1 from graph 0, first 1 to graph 1, first 1 nohead front lc rgb 'black' dt 2 lw 1.5
plot '../data/minife_10_increments.data' u ($5) lt rgb "#d95f02" title "Speedup Milan-X over Milan" axes x1y1, \
     '' u (($1-100)/10):($2/1000):xtic(1) w points lt 5 lw 2 lc rgb "#7570b3" title "AMD EPYC 7763 Milan        (256 MiB LLC)" axes x1y2, \
     '' u (($1-100)/10):($2/1000) w lines lw 1 dt 5 lc rgb "#7570b3" notitle axes x1y2, \
     '' u (($1-100)/10):($4/1000) w points lt 7 lw 2 lc rgb "#1b9e77" title "AMD EPYC 7773X Milan-X (768 MiB LLC)" axes x1y2, \
     '' u (($1-100)/10):($4/1000) w lines lw 1 dt 5 lc rgb "#1b9e77" notitle axes x1y2, \

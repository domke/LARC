set terminal svg size 1000,350 dynamic enhanced fname 'Times' butt dashlength 1.0
set output "../figures/gem5valid-cores.svg"

set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

set key left top opaque vertical Left reverse enhanced autotitles columnhead box width -1 font ",16"

set auto x
set xtics border in scale 1,0.5 nomirror norotate offset character 0, 0, 0 autojustify font ",24" offset 0,.3
set xrange [.5:32.5]
set xtics 1 font ",20"

set auto y
set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autojustify font ",22"
set yrange [0:1800]
set ytics 200

set xlabel "Number of OpenMP Threads" offset 2.5,0 font ",22"
set ylabel "STREAM Triad BW [in GB/s]" offset 0,1 font ",22"

#set arrow 1 from graph 0, first 800 to graph 1, first 800 nohead lc rgb 'black' dt 2 lw 2
#set label "LARC_{C}  L2 peak" at 31.8,900 right font ",18"
#set arrow 2 from graph 0, first 1600 to graph 1, first 1600 nohead lc rgb 'black' dt 2 lw 2
#set label "LARC^{A}  L2 peak" at 31.8,1700 right font ",18"

set arrow 1 from graph 0, first 1536 to graph 1, first 1536 nohead lc rgb 'black' dt 2 lw 2
set label "LARC's target L2 B/W" at 31.8,1636 right font ",18"

plot '../data/gem5valid-cores-arch3.data' u 1:2 w points lt 5 lw 2 lc rgb "#7570b3" title "LARC^A", \
     '' u 1:2 w lines lw 1 dt 5 lc rgb "#7570b3" notitle, \
     '../data/gem5valid-cores-arch2.data' u 1:2 w points lt 7 lw 2 lc rgb "#1b9e77" title "LARC_C", \
     '' u 1:2 w lines lw 1 dt 5 lc rgb "#1b9e77" notitle, \
     '../data/gem5valid-cores-arch0.data' u 1:2 w points lt 12 lw 3 lc rgb "#d95f02" title "AFX64_S", \
     '' u 1:2 w lines lw 1 dt 5 lc rgb "#d95f02" notitle, \
     '../data/gem5valid-cores-fugaku.data' u 1:2 w points lt 3 lw 3 lc rgb "#cc2325" title "AFX64 measured", \
     '' u 1:2 w lines lw 1 dt 5 lc rgb "#cc2325" notitle, \

set terminal svg size 1000,300 dynamic enhanced fname 'Times' butt dashlength 1.0
set output "../figures/l1simvali.svg"

set auto x
set style data histogram
set style histogram rowstacked title textcolor lt -1
set style fill solid 1.00 border lt -1
set key right top opaque vertical Left reverse enhanced autotitles columnhead box width -4 font ",16"

set boxwidth 0.8
set xtic font ",22" rotate by -35 scale 0 left offset -.5,0
set yrange [0:4]
set ytic 1 font ",20"
set ylabel "Relative Runtime" offset 1,0 font ",20"
set grid nopolar noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics y2tics nomy2tics nocbtics nomcbtics

set arrow 1 from graph 0, first 1 to graph 1, first 1 nohead front lc rgb 'black' dt 2 lw 1.5
plot "../data/l1simvali.data" using 2:xtic(1) lt 1 lc rgb "#56b4e9" title "MCA-based estimate"

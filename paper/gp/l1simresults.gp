set terminal svg size 1000,190 dynamic enhanced fname 'Times' butt dashlength 1.0

set auto x
set style data histogram
set style histogram rowstacked title textcolor lt -1
set style fill solid 1.00 border lt -1
#set key left top opaque vertical Left reverse enhanced autotitles columnhead box width -4 font ",12"

set boxwidth 0.8
set xtic font ",11" rotate by -45 scale 0 left offset -.5,0
set yrange [0:9]
set ytic 3 font ",12"
#set ytics add ('1' 1)
set ylabel "Speedup" offset 1,0 font ",12"
set grid nopolar noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics y2tics nomy2tics nocbtics nomcbtics

set output "../figures/l1simresults_top.svg"
set xrange [.5 : 62.5]

set label "20.0x" at 49.5,9.25 left rotate by 45 font ",11"
set label "13.1x" at 53.5,9.25 left rotate by 45 font ",11"
set arrow 1 from graph 0, first 1 to graph 1, first 1 nohead front lc rgb 'black' dt 2 lw 1.5

set label "PolyBench" at 5,8.1 left tc rgb "#797A79" font ",8"
set arrow 2 from .8,8.5 to 30.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "RIKEN TAPP" at 44,8.1 left tc rgb "#797A79" font ",8"
set arrow 3 from 30.8,8.5 to 52.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "NPB (OMP)" at 57,8.1 left tc rgb "#797A79" font ",8"
set arrow 4 from 52.8,8.5 to 62.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
plot "../data/l1simresults.data" using 2:xtic(1) lt 1 lw .5 lc rgb "#56b4e9" notitle #title "MCA-based estimate"

set output "../figures/l1simresults_bottom.svg"
set xrange [61.5 : 126.5]
set key center top opaque vertical Left reverse enhanced autotitles columnhead box width -4 font ",10"
set terminal svg size 1000,175 dynamic enhanced fname 'Times' butt dashlength 1.0

set label "34.8x" at 120.5,9.25 left rotate by 45 font ",11"
set arrow 5 from graph 0, first 1 to graph 1, first 1 nohead front lc rgb 'black' dt 2 lw 1.5

unset arrow 4
set label "NPB (MPI)" at 64,8.1 left tc rgb "#797A79" font ",8"
set arrow 6 from 61.8,8.5 to 70.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "TOP500, etc." at 71.2,8.1 left tc rgb "#797A79" font ",8"
set arrow 7 from 70.8,8.5 to 74.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "ECP" at 80,8.1 left tc rgb "#797A79" font ",8"
set arrow 8 from 74.8,8.5 to 85.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "Fiber" at 87,8.1 left tc rgb "#797A79" font ",8"
set arrow 9 from 85.8,8.5 to 92.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "SPEC CPU" at 107,8.1 left tc rgb "#797A79" font ",8"
set arrow 10 from 92.8,8.5 to 112.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
set label "SPEC OMP" at 114,8.1 left tc rgb "#797A79" font ",8"
set arrow 11 from 112.8,8.5 to 126.2,8.5 heads size .3,45 back lc rgb "#797A79" dt 1 lw .5
plot "../data/l1simresults.data" using 2:xtic(1) lt 1 lw .5 lc rgb "#56b4e9" title "MCA-based estimate"

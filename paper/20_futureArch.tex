\section{CPUs Empowered with High-capacity Cache: the Future of HPC?}\label{sec:projectedArch} %\fixme{~~page 2+3}

The memory bandwidth of modern systems has been the bottleneck (the ``memory wall''~\cite{mckee_reflections_2004}) ever since CPU performance started to outgrow the bandwidth of memory
subsystems in the early 1990s~\cite{mccalpin_memory_1995}. Today, this trend continues to shape the performance
optimization landscape in high-performance computing~\cite{or-bach_1000x_2017,oliveira_damov_2021}.
Diverse memory technologies are emerging to overcome said data movement bottleneck, such as
Processing-in-Memory (PIM)~\cite{balasubramonian_near-data_2014}, 3D-stackable High-Bandwidth Memory
(HBM)~\cite{mittal_survey_2016}, deeper (and more complex) memory hierarchies~\cite{warnock_41_2015},
and---the topic of the present paper---novel 3D-stacked
caches~\cite{loh_processor_2007,black_stacking_2006,shiba_96-mb_2021}.

In this study, our aspiration is to gauge the far end of processor technology and how we foresee it
to evolve in six to eight years from now, circa 2028, when processors using~\unit[1.5]{nm} technology
are expected to be available~\cite{ieee_irds_international_2021-1}. More specifically, as
3D-stacked SRAM memory~\cite{zhang_survey_2014} becomes more common, What are the performance implications
for common HPC workloads, and what new challenges lie ahead for the community? However, before attempting
to understand what performance may look like six years from now, we must describe how the processor
itself will change. In this section, we introduce, motivate, and reason about our design choices of
what we envision as a future processor that capitalizes on large capacity 3D-stacked cache, briefly
called \textbf{\proc{}~(LARge Cache processor)}. Before looking at \proc{}, we must first
set and analyze a baseline processor.


\subsection{\proc{}' Baseline: The A64FX Processor}
We choose to base our future processor design on the A64FX processor~\cite{yoshida_fujitsu_2018}.
The Arm-based A64FX processor from Fujitsu is currently powering Supercomputer Fugaku~\cite{sato_co-design_2020},
which, as of April 2022, leads the TOP500~\cite{strohmaier_top500_2021} (for HPL and HPCG;
cf.~Section~\ref{ssec:apps}) and Graph500 performance charts.
The A64FX is a many-core processor build on~\unit[7]{nm} technology, developed jointly by
Fujitsu and the RIKEN research institute. It has a total of 52 ARM-capable cores (including
Scalable Vector Extensions~\cite{stephens_arm_2017}) distributed across four internal compute
clusters, called Core Memory Groups (CMGs). Each CMG hosts 13 cores, where 12 are available
to the user, and one core is exclusively used for management. Each core has a local~\unit[64]{KiB}
instruction and data-cache, and is capable of delivering~\unit[70.4]{Gflop/s} (IEEE-754 double-precision)
performance. Each CMG also contains one of the four slices of the processor's~\unit[32]{MiB}
L2 cache, delivering over~\unit[900]{GB/s} bandwidth to the CMG~\cite{yoshida_fujitsu_2018}.
The L2 cache, which is the CPU's last level cache (LLC), is kept coherent through a ring interconnect that connects
the four CMGs. Inside the CMG, a crossbar switch is used to connect the cores and the L2 slice.
The cache line-size is 256 bytes, and the bus-width between the L1 and L2 cache is set to be
128 bytes (read) and 64 bytes (write). The L2 cache has 16-way set associativity, and the total
performance per CMG is~\unit[845]{Gflop/s} (user cores) or~\unit[3.4]{Tflop/s} for the entire chip.

We emphasize that our aim is not
to propose a successor of A64FX, nor are we particularly restricting our vision by the design
constrains of A64FX (e.g., power budget). However, we build our design on A64FX because:
\textbf{(i)}~as mentioned above, A64FX represents the pinnacle of CPU performance in commercially
available CPUs, so it is natural to use it as a starting point.
\textbf{(ii)}~A64FX is the only commercially-available CPU, currently in continued production, with HBM. The expected bandwidth ratio between future HBM and future 3D-stacked caches is similar
to the ratio between traditional DRAM and LLC bandwidths~\cite{nori_criticality_2018},
which is what applications and performance models are accustomed to.
\textbf{(iii)}~The A64FX LLC cache design (particularly the L2 slices connected by a crossbar switch)
happens to be convenient and thus, requires a minimal effort to extend the design in a
simulated environment.


In conclusion, while we extend the A64FX architecture, our workflow itself can be generalized to
cover any of the processors supported by CPU simulators (e.g., variants of gem5~\cite{binkert_gem5_2011}
can simulate other architectures, including x86).


\subsection{Floorplan Analysis for Fujitsu A64FX}
In order to estimate the floorplan of the future \proc{} processor built on~\unit[1.5]{nm} technology,
we first need the floorplan of the current A64FX processor built at~\unit[7]{nm}; but detailed floorplan
information is very sparse and often not readily available. Fortunately, we do know that the die size
of A64FX is~\mbox{$\approx\,$\unit[400]{\sqmm}}~\cite{sato_co-design_2020}.
Given this knowledge, and the openly-available die shots with processor core segments
highlighted~\cite{okazaki_supercomputer_2020}, we can estimate most of the A64FX floorplan,
including the size of CMGs and processor cores. We performed this exercise, and the result is shown in
Figure~\ref{fig:n64_picture}. Overall, each CMG is~$\approx\,$\unit[48]{\sqmm} in area, where a A64FX
core occupies $\approx\,$\unit[2.25]{\sqmm} area. The remaining parts of the CMG consist of the
L2 cache slice and controller and the interconnect for intra-CMG communication.



\subsection{From A64FX's CMG Layout to \proc{}'s CMG Layout}
Knowing the floorplan, we now proceed to describe how we envision the CMG design
with~\unit[1.5]{nm} technology. We scale the CMG by moving three generations, from~\unit[7]{nm}
to~\unit[1.5]{nm}, and reduce the silicon footprint by around~8x ($\approx\,$2x per generation)
for the entire CMG~\cite{esmaeilzadeh_dark_2011}. The new CMG consumes as little
as~\unit[6]{\sqmm} of silicon area. Next, we reclaim the area of the CMG currently occupied by
the L2 cache and controller and replace it with three additional CPU cores, yielding a
total of 16 per CMG. Further, inline with the projected year 2019$\rightarrow$2028
growth in the number of cores~\cite{ieee_irds_international_2021}, we double the core count of
the CMG to 32, which leads to it occupying $\approx\,$\unit[12]{\sqmm} of silicon area.
We pessimistically leave the area for the interconnect the same and continue to use it as the
primary means for communication with the CMG. We call this new variant as \proc{}'s CMG.
Finally, we conservatively assume the same die size, and therefore, \proc{} would have 16 CMGs,
each with 32 cores, in comparison to A64FX's 4 CMG with 12+1 cores each. In \proc{}, we ignore the management core.
However, our performance analysis will remain on the CMG level, instead of full chip, due to
limitations which we detail in Section~\ref{ssec:gem5}.

\subsection{\proc{}'s Vertically Stacked Cache}\label{ssec:n64fx}
In the above design, we removed the L2 cache and controller from the CMG of \proc{}.
We now assume that the L2 cache can be directly placed vertically on the CMG through 3D
stacking~\cite{loh_processor_2007}.
We build our
estimations based on experiments from Shiba et al.~\cite{shiba_96-mb_2021}, who
demonstrate the feasibility of stacking up-to-eight SRAM dies on top of a processor using a ThruChip Interface (TCI).
The capacity and bandwidth of stacked memory is a function of several parameters: the number of channels available ($N_\text{ch}$),
the per-channel capacity ($N_\text{cap}$ in~\unit[]{KiB}), their width ($W$ in bytes), the number of stacked dies ($N_\text{dies}$), and the operating frequency ($f_\text{clk}$ in~\unit[]{GHz}).
Shiba et al.~\cite{shiba_96-mb_2021} estimated that at a~\unit[10]{nm} process technology, eight stacks would provide $\approx\,$\unit[512]{MiB} of
aggregated SRAM capacity for a footprint of $\approx\,$\unit[121]{\sqmm}. In their design, each stack has 128 channels of \unit[512]{KiB} capacity.
In our work, we conservatively assume an 8x scaling from~\unit[10]{nm} to~\unit[1.5]{nm}, and thus, at \unit[12]{\sqmm} area (the size of one
\proc{} CMG), $N_\text{ch}$ on each die would be $\approx\,$102 (=128*8/10). Keeping the area and total capacity fixed, we reduce per-channel
capacity by 4x and increase the number of channels by 4x, thus, $N_\text{ch}=\text{408}$. We approximate it a nearby sum of power-of-two number, viz., $N_\text{ch}=\text{384}$.
Thus, with eight stacked dies ($N_\text{dies}=\text{8}$), our 3D SRAM cache has a total storage capacity of $N_\text{dies} \cdot N_\text{ch} \cdot N_\text{cap}=\text{\unit[384]{MiB}}$ per CMG.
We estimate the bandwidth in a similar way. We know from previous studies~\cite{shiba_96-mb_2021}
that 3D-stacked SRAM, built on~\unit[40]{nm} technology, can operate at~\unit[300]{MHz}.
We conservatively expect the same SRAM to operate at ($f_\text{clk}$=)\unit[1]{GHz} when moving
from~\unit[40]{nm}$\rightarrow$\unit[1.5]{nm}. Each channel's width is given in~\cite{shiba_96-mb_2021}
and amounts to ($W$=)4 byte (32-bit). With this, the CMG bandwidth becomes:
$N_\text{ch} \cdot f_\text{clk} \cdot W = \text{\unit[1536]{GB/s}}$.
The read- and
write-latency of this SRAM cache  is 3 cycles \cite{shiba_96-mb_2021}.



\begin{figure}[tbp]
    \centering
    \includegraphics[width=.74\linewidth]{figures/n64fx}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-1ex}}
    \caption{Difference between A64FX's Core Memory Group (CMG) and a \proc{} CMG in various performance-governing parameters; Most notable (for our study) is the~48x increase in per-CMG L2 cache capacity; \textbf{Note:} despite appearing similar in the figure, the \proc{} CMG is, in fact, four times smaller}
    \label{fig:n64_picture}
    \iftoggle{releaseOnArxivWithoutAckno}{}{\vspace{-3ex}}
\end{figure}


A challenge in the use of large caches is their large tag size and high bandwidth consumption.
Several solutions to this issue have been proposed in the context of stacked-DRAM
caches~\cite{mittal_survey_2016}. For our cache design, we assume a~\unit[256]{B} cache block
design, which avoids bandwidth bloat. Each tag takes~\unit[6]{B} and as such, the total tag array
size for each CMG becomes~\unit[9]{MiB}. This tag array can be easily placed in the cache itself. We
assume that tag and data accesses happen sequentially. The tags and data of a cache set are
stored on a single die. Hence, on every access, only one die needs to be activated. Since
this takes only few cycles, the overall miss penalty remains small and comparable to that of A64FX' LLC.

To show that our cache projections are realistic, we compare it with AMD's 3D V-cache
design. It uses a single stacked die providing~\unit[64]{MiB} capacity (in addition to
the~\unit[32]{MiB} cache in the base die) at~\unit[7]{nm}~\cite{cutress_amd_2021,evers_amd_2022},
and only 3 to 4 cycles of extra latency~\cite{cheese_amds_2022}. It has~\unit[36]{\sqmm} area
and has a bandwidth of~\unit[2]{TB/s}. When stacking additional dies on top, and assuming an 8x scaling of
the area by going from~\unit[7]{nm} to~\unit[1.5]{nm}, we speculate that the LLC capacity of
this commercial processor could easily exceed that of our proposed \proc{}.

\subsection{\proc{}'s Core Memory Group (CMG)}
At last, we detail our experimental CMG built on a hypothetical~\unit[1.5]{nm}
technology: the \textbf{\proc{}~CMG}.
An illustration  of this system is shown in
Figure~\ref{fig:n64_picture}. Each CMG consists of 32 A64FX-like cores, which keeps the
L1 instruction- and data-cache to~\unit[64]{KiB} each, yielding a per CMG performance of
$\approx\,$\unit[2.3]{Tflop/s} (IEEE-754 double-precision). A~\unit[384]{MiB} L2 cache is
stacked vertically on the top of the CMG through eight SRAM layers.
We also keep the bandwidth of external HBM memory to its current A64FX value of~\unit[256]{GB/s}
to be able to quantify performance improvements from the proposed large capacity
3D cache in isolation from any improvements that would come from increased HBM bandwidth.

To summarize, a complete, hypothetical \proc{} chip would contain 512 processing cores,
\unit[6]{GiB} of stacked L2 cache, a peak L2 bandwidth of~\unit[24.6]{TB/s}, a peak HBM
bandwidth of~\unit[4.1]{TB/s}, and~\unit[36]{Tflop/s} of raw, double-precision, compute.


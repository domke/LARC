#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mkl_cblas.h>
#define blasint MKL_INT
#define BLASFN(FUNC) cblas_##FUNC

#include <ittnotify.h>
#include <signal.h>
#include <stdlib.h>
#define STARTSDE(go,rank) {if(go && 0==rank && getenv("PCMPID")) kill(atoi(getenv("PCMPID")),SIGUSR1); __itt_resume(); __SSC_MARK(0x111);}
#define STOPSDE(go,rank) {__SSC_MARK(0x222); __itt_pause(); if(go && 0==rank && getenv("PCMPID")) kill(atoi(getenv("PCMPID")),SIGUSR1);}

/* Define sample dataset sizes. */
#  ifdef MINI_DATASET
#   define NI 20
#   define NJ 25
#   define NK 30
#  endif

#  ifdef SMALL_DATASET
#   define NI 60
#   define NJ 70
#   define NK 80
#  endif

#  ifdef MEDIUM_DATASET
#   define NI 200
#   define NJ 220
#   define NK 240
#  endif

#  ifdef LARGE_DATASET
#   define NI 1000
#   define NJ 1100
#   define NK 1200
#  endif

#  ifdef EXTRALARGE_DATASET
#   define NI 2000
#   define NJ 2300
#   define NK 2600
#  endif

#define TIME_GEMM_START(maj, tA, tB, m, n, k, alp, bet, lA, lB, lC)             \
    fprintf(stderr,                                                             \
            "dgemm: %s T,T=(%d,%d) M,N,K=(%d,%d,%d) alp,bet=(%e,%e),"           \
            " ld{A,B,C}=(%d,%d,%d)\n",                                          \
            #maj, tA, tB, m, n, k, alp, bet, lA, lB, lC);                       \
    struct timespec __gemm_start__, __gemm_finish__;                            \
    clock_gettime(CLOCK_REALTIME, &__gemm_start__);

#define __GIGA__ ((double)1e9)
#define __StoNS__ 1e9

#define TIME_GEMM_STOP(para, r, m, n, k)                                        \
    clock_gettime(CLOCK_REALTIME, &__gemm_finish__);                            \
    long __gemm_sec__ = __gemm_finish__.tv_sec - __gemm_start__.tv_sec;         \
    long __gemm_ns__ = __gemm_finish__.tv_nsec - __gemm_start__.tv_nsec;        \
    if (__gemm_start__.tv_nsec > __gemm_finish__.tv_nsec) {                     \
        --__gemm_sec__;                                                         \
        __gemm_ns__ += __StoNS__;                                               \
    }                                                                           \
    const double                                                                \
            __gemm_time__ = __gemm_sec__ + (__gemm_ns__ / (double)__StoNS__),   \
            _r_ = r, _m_ = m, _n_ = n, _k_ = k, _2mnk_ = 2.0 * _m_ * _n_ * _k_; \
    fprintf(stderr, "dgemm (%s) in gflop/s: %f (flop/byte: %f; rt:%0.15lf s)\n",\
            #para, (_2mnk_ / __GIGA__) / (__gemm_time__ / _r_),                 \
            _2mnk_ / (4.0 * (_m_ * _n_ + _m_ * _k_ + _n_ * _k_)),               \
            (__gemm_time__ / _r_));


typedef struct gemm {
    char tA, tB;
    blasint m,n,k;
    blasint lda, ldb, ldc;
    double alpha, beta;
} dgemm_in_t;

int main() {
    int rounds = 1, heat = 10, in=1;

    dgemm_in_t dgemm_input[in]; in=0;
    dgemm_input[in++] = (dgemm_in_t){'n', 'n', NI, NJ, NK, NI, NK, NI,
                                     1.000000e+00, 1.000000e+00};

    for (int i=0; i<in; i++) {
        CBLAS_TRANSPOSE tA = (dgemm_input[i].tA == 'n') ? CblasNoTrans : CblasTrans,
                        tB = (dgemm_input[i].tB == 'n') ? CblasNoTrans : CblasTrans;
        blasint m = dgemm_input[i].m, n = dgemm_input[i].n, k = dgemm_input[i].k;
        blasint lda = dgemm_input[i].lda, ldb = dgemm_input[i].ldb,
                ldc = dgemm_input[i].ldc;
        double alpha = dgemm_input[i].alpha, beta = dgemm_input[i].beta;

        // INIT
        double *A, *B, *C;
        A = (double *)malloc(sizeof(double)*m*k);
        B = (double *)malloc(sizeof(double)*k*n);
        C = (double *)malloc(sizeof(double)*m*n);
        {
        for (int j=0; j<m*k; j++) A[j] = 0.1;
        for (int j=0; j<k*n; j++) B[j] = -0.1;
        for (int j=0; j<m*n; j++) C[j] = j;
        }

        {
        // HEAT
        TIME_GEMM_START(CblasColMajor, tA, tB, m, n, k, alpha, beta, lda, ldb, ldc);
        for (int j=0; j<heat; j++)
            BLASFN(dgemm)(CblasColMajor, tA, tB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
        TIME_GEMM_STOP(pre, heat, m, n, k);
        }

        // MEASURE
        TIME_GEMM_START(CblasColMajor, tA, tB, m, n, k, alpha, beta, lda, ldb, ldc);
        STARTSDE(1,0);
        for (int j=0; j < rounds; j++)
            BLASFN(dgemm)(CblasColMajor, tA, tB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
        STOPSDE(1,0);
        TIME_GEMM_STOP(core, rounds, m, n, k);

        free(A);
        free(B);
        free(C);
    }
}

for SIZE in MINI SMALL MEDIUM LARGE EXTRALARGE ; do icc -D${SIZE}_DATASET -o gemm_${SIZE} -I${ADVISOR_2018_DIR}/include -O3 -xHost -ipo ./poly_gemm_wMKL.c -L${ADVISOR_2018_DIR}/lib64 -littnotify -mkl -static -static-intel; done
#move binaries: mv gemm_* ../polybench/linear-algebra/blas/gemm/
#exec ./run/polybench/best.sh and ./run/polybench/prof.sh
#/bin/grep 'dgemm (core' gemm_EXTRALARGE.log | cut -d':' -f4 | cut -d' ' -f1 | sort -g | head

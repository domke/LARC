#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
source ${ROOTDIR}/conf/host.cfg "gem5"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "gem5"
load_compiler_env "${COMP}"

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
Target_OMP_NUM_THREADS=12
ReqArch=1

LOGDIR="$(dirname ${SELF})/gemtesthbm"; mkdir -p "${LOGDIR}"
if [[ "$(hostname -s)" = "kiev0" ]]; then
	NELEMinKB=24552
	NELEM=1047552
	Y=4; for y in `seq 1 7`; do Y=$((Y*2)); X=4; for x in `seq 1 $y`; do X=$((X*2));
	for ReqArch in 1; do
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") --mem_bus_width=$X --mem_resp_width=$Y -c "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1
			echo mem_bus_width=$X mem_resp_width=$Y $(/bin/grep ' BW: ' ${LOG} | tail -1)
	done
	done; done
	wait
fi

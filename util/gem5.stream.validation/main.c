#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>

# define  KB (1024ULL)
# define  MB (KB*KB)
# define  GB (MB*KB)

double get_dtime() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ((double)(ts.tv_sec) + (double)(ts.tv_nsec) * 1e-9);
}

#define NELEM(array)   (sizeof(array)/sizeof(array[1]))

#define TIMER_ARGS int *, int *, int *, double *, double *, double *

typedef void (*kernel_t)(TIMER_ARGS);

struct kernel_param {
  int id;
  int kn;
  kernel_t  timer;
  char *kernel;
  int nflop;
  int iter;
  int loops;
  int dist;
};

/* ----- args -------------------------------------------------------- */
#ifdef __N__
#define N __N__
#else
#define N 10485760
#endif

#ifdef __I__
#define DEF_ITER __I__
#else
#define DEF_ITER 100
#endif

int     g_iter, g_len, g_dist;
double g_y[N]  __attribute__((aligned(1024)));
double g_x1[N] __attribute__((aligned(1024)));
double g_x2[N] __attribute__((aligned(1024)));

/* ----- kernels ----------------------------------------------------- */

/* kernel prototype */
void calc04_fmadd_r8_(TIMER_ARGS);

struct kernel_param kn_param[]= {
{ 4, 4, calc04_fmadd_r8_,"04", 1, 1, N, 1 },
};

/* ------------------------------------------------------------------- */

int setup_kernel(struct kernel_param *kn)
{
  int i;

  g_iter  = kn->iter;
  g_len   = kn->loops;
  g_dist  = kn->dist;

#pragma omp parallel for default(shared) private(i) schedule(static)
  for(i=0; i < g_len*g_dist; i++){
    g_y[i]  = 0.0;
    g_x1[i] = 3.3;
    g_x2[i] = 5.5;
  }
  return 0;
}

int main(int argc, char *argv[])
{
  int i;
  int dummy = 1;
  double elaps, cpl, gflops;
  int rep;

  if (argc > 1)
    rep = atoi(argv[1]);
  else
    rep = DEF_ITER;
  kn_param[0].iter = rep;

  for(i=0; i < NELEM(kn_param); i++) {
    struct kernel_param *kn = &kn_param[i];

    setup_kernel(kn);

    //preheat
    kn->timer(&g_len, &dummy,  &g_dist, g_y, g_x1, g_x2);

    //timed fn
    elaps = get_dtime();
    kn->timer(&g_len, &g_iter, &g_dist, g_y, g_x1, g_x2);
    elaps = get_dtime() - elaps;
  }
  printf("rep: %d, n: %d\n", g_iter, g_len);
  gflops = (double)(g_len) * g_iter * 2.0 / elaps / 1.0e9;
  cpl = elaps * 2.0e9 / g_iter / g_len * 8;
  printf("g_y:%08x, g_x1:%08x, g_x2:%08x\n", g_y, g_x1, g_x2);

  printf("global elaps: %.6lf sec, %.2lf cycle/8loop, %.2lf GFLOPS, BW: %.2lf GB/s\n",
	 elaps, cpl, gflops, gflops / 2 * 3 * 8);
  printf("GB/s:%.4lf\n", gflops / 2 * 3 * 8);

  return 0;
}


#!/bin/bash

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
mkdir -p log.fugaku/

for Target_OMP_NUM_THREADS in 12; do
	for POW in `seq 1 20`; do
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="log.fugaku/log.${BINARY}"
			rm -f ${LOG}
			for c in `seq 0 9`; do
				OMP_NUM_THREADS=${Target_OMP_NUM_THREADS} OMP_BIND=close \
					numactl -C 12-$((11+${Target_OMP_NUM_THREADS})) ./bin/${BINARY} >> ${LOG} 2>&1
			done
			BEST="$(/bin/grep '^GB' ${LOG} | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; echo -e "\n\nBest(${NELEMinKB}): ${BEST} GB/s" >> ${LOG}
		done
	done
done


#!/bin/bash

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
mkdir -p log.fugaku/

# one graph to strong scale L2 bandwidth with core count
for Target_OMP_NUM_THREADS in `seq 1 32`; do
	# 8MB /12core /3vec ~= 227KB -> use 128 to stay in L2
	TARGETinKB="$(( ${Target_OMP_NUM_THREADS} * ${NumVectors} * 128 ))"
	TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"
	NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
	NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
	NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
	BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="log.fugaku/log.${BINARY}"
	rm -f ${LOG}
	for c in `seq 0 9`; do
		OMP_NUM_THREADS=${Target_OMP_NUM_THREADS} OMP_BIND=close \
			numactl -C 12-$((11+${Target_OMP_NUM_THREADS})) ./bin/${BINARY} >> ${LOG} 2>&1
	done
	BEST="$(/bin/grep '^GB' ${LOG} | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; echo -e "\n\nBest(${NELEMinKB}): ${BEST} GB/s" >> ${LOG}
done

# one graph to test L1/L2/HBM throughput on 1, 12, 32 cores
for Target_OMP_NUM_THREADS in 1 12 24 32; do
	for POW in `seq 0 20`; do	# from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="log.fugaku/log.${BINARY}"
		rm -f ${LOG}
		for c in `seq 0 9`; do
			OMP_NUM_THREADS=${Target_OMP_NUM_THREADS} OMP_BIND=close \
				numactl -C 12-$((11+${Target_OMP_NUM_THREADS})) ./bin/${BINARY} >> ${LOG} 2>&1
		done
		BEST="$(/bin/grep '^GB' ${LOG} | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; echo -e "\n\nBest(${NELEMinKB}): ${BEST} GB/s" >> ${LOG}
	done
	for POW in `seq 2 20`; do       # intermediate steps from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="log.fugaku/log.${BINARY}"
		rm -f ${LOG}
		for c in `seq 0 9`; do
			OMP_NUM_THREADS=${Target_OMP_NUM_THREADS} OMP_BIND=close \
				numactl -C 12-$((11+${Target_OMP_NUM_THREADS})) ./bin/${BINARY} >> ${LOG} 2>&1
		done
		BEST="$(/bin/grep '^GB' ${LOG} | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; echo -e "\n\nBest(${NELEMinKB}): ${BEST} GB/s" >> ${LOG}
	done
done


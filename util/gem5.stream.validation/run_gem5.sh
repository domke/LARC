#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
source ${ROOTDIR}/conf/host.cfg "gem5"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "gem5"
load_compiler_env "${COMP}"
LOGDIR="$(dirname ${SELF})/log.gem5"

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70

if [[ "$(hostname -s)" = "kiev5" ]]; then
	for ReqArch in 0 -2 -3; do
		# one graph to strong scale L2 bandwidth with core count
		for Target_OMP_NUM_THREADS in `seq 1 16`; do
			# 8MB /12core /3vec ~= 227KB -> use 128 to stay in L2
			TARGETinKB="$(( ${Target_OMP_NUM_THREADS} * ${NumVectors} * 128 ))"
			TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
		for Target_OMP_NUM_THREADS in `seq 17 32`; do
			# 8MB /12core /3vec ~= 227KB -> use 128 to stay in L2
			TARGETinKB="$(( ${Target_OMP_NUM_THREADS} * ${NumVectors} * 128 ))"
			TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
	done
fi

ReqArch=0
# one graph to test L1/L2/HBM throughput on 1, 12, 32 cores
for Target_OMP_NUM_THREADS in 1 12 24 32; do
	if [[ "$(hostname -s)-${Target_OMP_NUM_THREADS}" = "kiev1-1" ]] || [[ "$(hostname -s)-${Target_OMP_NUM_THREADS}" = "kiev2-12" ]] || [[ "$(hostname -s)-${Target_OMP_NUM_THREADS}" = "kiev3-24" ]] || [[ "$(hostname -s)-${Target_OMP_NUM_THREADS}" = "kiev4-32" ]]; then
		for POW in `seq 0 20`; do	# from 1 KB to 1 GB
			TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
		for POW in `seq 2 20`; do       # intermediate steps from 1 KB to 1 GB
			TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
	fi
done

Target_OMP_NUM_THREADS=32
if [[ "$(hostname -s)" = "kiev0" ]]; then
	for ReqArch in -2 -3; do
		for POW in `seq 0 20`; do	# from 1 KB to 1 GB
			TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
		for POW in `seq 2 20`; do       # intermediate steps from 1 KB to 1 GB
			TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"; rm -f ${LOG}
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
		wait
	done
fi


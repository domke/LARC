#!/bin/bash

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
Target_OMP_NUM_THREADS=1
ReqArch=1
LOGDIR="./log.fugaku.nopref.omp1"
for POW in `seq 1 10`; do
	for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sorted(sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5))]))"); do
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}
	done
done

subroutine calc04_fmadd_r8(n,iter,dist,y,x1,x2)
  real*8 c0,y(n),x1(n),x2(n)
  integer n,iter,i,j,dist
  c0 = x2(1)

!!!  call fapp_start("foo",1,0)
!$omp parallel default(shared) private(j) firstprivate(c0)
  do j = 1, iter
!$omp do private(i) schedule(static)
     do i = 1, n
        y(i) = x1(i) + c0 * x2(i)
     end do
!$omp end do nowait
  enddo
!$omp end parallel
!!!  call fapp_stop("foo",1,0)
end subroutine calc04_fmadd_r8


#!/bin/bash

Target_OMP_NUM_THREADS="${Target_OMP_NUM_THREADS:-12}"
TARGETinKB="${TARGETinKB:-4}"
TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3

# values from p.215 https://www.fugaku.r-ccs.riken.jp/doc_root/en/programming_guides/Tuning_Programming_Guide_v1.4.pdf
# use XOS_MMM_L_PAGING_POLICY="demand:demand:demand" for large page and OMP_NUM_THREADS>12
for _ZFILL in 18; do #`seq 8 2 32`; do
	for _PREL1 in 7; do #9; do #`seq 6 1 16`; do
		for _PREL2 in 70; do #`seq 32 4 64`; do
			_NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			_NELEM="$(( (${_NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
			NELEM=${_NELEM} ZFILL=${_ZFILL} PREL1=${_PREL1} PREL2=${_PREL2} OMP=${Target_OMP_NUM_THREADS} make -B
		done
	done
done

# one graph to strong scale L2 bandwidth with core count
for Target_OMP_NUM_THREADS in `seq 1 32`; do
	# 8MB /12core /3vec ~= 227KB -> use 128 to stay in L2
	TARGETinKB="$(( ${Target_OMP_NUM_THREADS} * ${NumVectors} * 128 ))"
	TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"
	_NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
	_NELEM="$(( (${_NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
	NELEM=${_NELEM} ZFILL=18 PREL1=7 PREL2=70 OMP=${Target_OMP_NUM_THREADS} make -B
done

# one graph to test L1/L2/HBM throughput on 1, 12, 32 cores
for Target_OMP_NUM_THREADS in 1 12 24 32; do
	for POW in `seq 0 20`; do	# from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
		_NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		_NELEM="$(( (${_NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEM=${_NELEM} ZFILL=18 PREL1=7 PREL2=70 OMP=${Target_OMP_NUM_THREADS} make -B
	done
	for POW in `seq 2 20`; do       # intermediate steps from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
		_NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		_NELEM="$(( (${_NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEM=${_NELEM} ZFILL=18 PREL1=7 PREL2=70 OMP=${Target_OMP_NUM_THREADS} make -B
	done
done

rm -f bin/triad.0KB.*

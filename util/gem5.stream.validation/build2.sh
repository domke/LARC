#!/bin/bash

Target_OMP_NUM_THREADS="${Target_OMP_NUM_THREADS:-12}"
TARGETinKB="${TARGETinKB:-4}"
TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3

# one graph to test L1/L2/HBM throughput on 1, 12, 32 cores
for Target_OMP_NUM_THREADS in 12; do #1 12 24 32; do
	for POW in `seq 1 20`; do	# from 1 KB to 1 GB
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			_NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEM=${_NELEM} ZFILL=18 PREL1=7 PREL2=70 OMP=${Target_OMP_NUM_THREADS} make -B
		done
	done
done

rm -f bin/triad.0KB.*

#!/bin/bash

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
export ROOTDIR="$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../)"
source ${ROOTDIR}/conf/host.cfg "gem5"
source ${ROOTDIR}/conf/env.cfg
get_comp_env_name "gem5"
load_compiler_env "${COMP}"

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
Target_OMP_NUM_THREADS=12
ReqArch=1
LOGDIR="$(dirname ${SELF})/log.gem5.kprefe"; mkdir -p "${LOGDIR}"
if [[ "$(hostname -s)" = "kiev0" ]]; then
	for POW in `seq 1 2 20`; do
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin.kprefe/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
	done
	wait
	for POW in `seq 2 2 20`; do
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin.kprefe/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
	done
	wait
fi

LOGDIR="$(dirname ${SELF})/log.gem5.nopref"; mkdir -p "${LOGDIR}"
if [[ "$(hostname -s)" = "kiev0" ]]; then
	for POW in `seq 1 2 20`; do
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin.nopref/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
	done
	wait
	for POW in `seq 2 2 20`; do
		for TARGETMEMSIZE in $(python3 -c "from random import sample, seed; seed(0); print(' '.join([str(elem) for elem in sample(range(1024*2**$((${POW}-1)),1024*2**${POW}), k=5)]))"); do
			NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
			NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
			BINARY="triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"; LOG="${LOGDIR}/log.RA${ReqArch}.${BINARY}"
			$(get_gem5_cmd "1" "${Target_OMP_NUM_THREADS}" "${LOG}" "" "${ReqArch}" "") "$(dirname ${SELF})/bin.nopref/${BINARY}" -o "${INPUT}" >> ${LOG} 2>&1 &
		done
	done
	wait
fi

#!/bin/bash

CacheLineSizeinByte=256
BytePerElem=8
NumVectors=3
ZFILL=18
PREL1=7
PREL2=70
LOGDIR="./log.gem5"

for ReqArch in 0 -2 -3; do
	echo -e "\n\n\nReqArch=${ReqArch}"
	# one graph to strong scale L2 bandwidth with core count
	for Target_OMP_NUM_THREADS in `seq 1 32`; do
		# 8MB /12core /3vec ~= 227KB -> use 128 to stay in L2
		TARGETinKB="$(( ${Target_OMP_NUM_THREADS} * ${NumVectors} * 128 ))"
		TARGETMEMSIZE="$((1024 * ${TARGETinKB}))"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.RA${ReqArch}.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}
	done
done

ReqArch=0
echo -e "\n\n\nReqArch=${ReqArch}"
# one graph to test L1/L2/HBM throughput on 1, 12, 32 cores
for Target_OMP_NUM_THREADS in 1 12 24 32; do
	for POW in `seq 0 20`; do	# from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.RA${ReqArch}.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}

		TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.RA${ReqArch}.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}
	done
done

Target_OMP_NUM_THREADS=32
for ReqArch in -2 -3; do
	echo -e "\n\n\nReqArch=${ReqArch}"
	for POW in `seq 0 20`; do	# from 1 KB to 1 GB
		TARGETMEMSIZE="$(echo "1024*(2^(${POW}-1)+2^(${POW}-2))" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.RA${ReqArch}.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}

		TARGETMEMSIZE="$(echo "1024*2^${POW}" | bc)"
		NELEM="$(( ${TARGETMEMSIZE} / ${NumVectors} / ${BytePerElem} ))"
		NELEM="$(( (${NELEM} / ${CacheLineSizeinByte} / ${Target_OMP_NUM_THREADS}) * ${CacheLineSizeinByte} * ${Target_OMP_NUM_THREADS} ))"
		NELEMinKB="$(( ${NELEM} * ${BytePerElem} * ${NumVectors} / 1024 ))"
		LOG="${LOGDIR}/log.RA${ReqArch}.triad.${NELEMinKB}KB.${NELEM}.${ZFILL}.${PREL1}.${PREL2}.omp${Target_OMP_NUM_THREADS}"
		BEST="$(/bin/grep '^GB' ${LOG} 2>&1 | awk -F 'GB/s:' '{print $2}' | sort -g | tail -1)"; if [ -z ${BEST} ]; then BEST="-"; fi
		echo ${Target_OMP_NUM_THREADS} ${NELEMinKB} ${BEST}
	done
done


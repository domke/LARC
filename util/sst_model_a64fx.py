#!/usr/bin/python3

import sst
from os import path, environ
from sys import exit
from argparse import ArgumentParser
from configparser import ConfigParser


class SimCfgParser:
    def __init__(self, cfg_file, **kwargs):
        cp = ConfigParser()
        if not cp.read(cfg_file):
            raise Exception('Unable to read file "%s"' % cfg_file)

        self.verbose = 'verbose' in kwargs and kwargs['verbose']

        self.comp = self._parseComponents(cp, 'Components')
        self.core = self._parseCore(cp, 'Core')
        self.cmg = self._parseCMG(cp, 'CMG')
        self.socket = self._parseSocket(cp, 'Socket')

        self.total_cores = self.socket['num_cmg'] * self.cmg['num_cores']

        self.cache = dict()
        self.cache['L1Cache'] = self._parseCache(cp, 'L1Cache')
        self.cache['L1Prefetcher'] = self._parseCachePrefetcher(cp, 'L1Cache')
        self.cache['L2Cache'] = self._parseCache(cp, 'L2Cache')
        self.cache['L2Prefetcher'] = self._parseCachePrefetcher(cp, 'L2Cache')

        self.mem = self._parseMainMemory(cp, 'MainMemory', self.comp['mainmemory'])
        self.mem_ctrl = self._parseMemController(cp, 'MemController')
        self.dir_ctrl = self._parseDirectoryController(cp, 'DirectoryController')

        self.workload = self._parseWorkload(cp, None, self.comp['workload'])


    def _parseComponents(self, cp, section):
        cp['DEFAULT'] = dict({'workload': 'ariel',                              #program to drive the simulation
                              'networkonchip': 'merlin.hr_router',              #network to connect CMGs
                              'mainmemory': 'dramsim3',                         #sst component representing the dram/hbm/...
                              })
        comp = dict()
        for key in cp['DEFAULT']:
            comp[key] = cp.get(section, key)
        #
        return comp

    def getComponentsCfg(self):
        return self.comp


    def _parseCore(self, cp, section):
        cp['DEFAULT'] = dict({'clock': '1GHz',
                              'max_reqs_cycle': 1,
                              })
        core = dict()
        for key in cp['DEFAULT']:
            core[key] = cp.get(section, key)
        #
        return core

    def getCoreCfg(self, core_id):
        return self.core


    def _parseCMG(self, cp, section):
        cp['DEFAULT'] = dict({'num_cores': 12,
                              'mem_size': '8GiB',
                              'num_mem_ctrl': 1,
                              })
        cmg = dict()
        for key in cp['DEFAULT']:
            cmg[key] = cp.get(section, key)
        #
        return cmg

    def getCMGCfg(self, core_id):
        return self.cmg


    def _parseSocket(self, cp, section):
        cp['DEFAULT'] = dict({'num_cmg': 1,
                              })
        socket = dict()
        for key in cp['DEFAULT']:
            socket[key] = cp.get(section, key)
        #
        return socket

    def getSocketCfg(self, core_id):
        return self.cmg


    def _parseCache(self, cp, section):
        cp['DEFAULT'] = dict({'cache_frequency': self.core['clock'],            #[<req>]Clock frequency or period with units (Hz or s; SI units OK). For L1s, this is usually the same as the CPU's frequency.
                              'cache_size': '4KiB',                             #[<req>]Cache size with units. Eg. 4KiB or 1MiB
                              'associativity': 4,                               #[<req>]Associativity of the cache. In set associative mode, this is the number of ways.
                              'access_latency_cycles': 100,                     #[<req>]Latency (in cycles) to access the cache data array. This latency is paid by cache hits and coherence requests that need to return data.
                              'L1': 1 if 'L1' in section else 0,                #[<req>]Required for L1s, specifies whether cache is an L1. Options: 0[not L1], 1[L1]
                              'node': 0,                                        #[<req>]Node number in multinode evnironment
                              'cache_line_size': 64,                            #Size of a cache line (aka cache block
                              'coherence_protocol': 'MESI',                     #Coherence protocol. Options: MESI, MSI, NONE
                              'cache_type': 'inclusive',                        #Cache type. Options: inclusive cache ('inclusive', required for L1s), non-inclusive cache ('noninclusive') or non-inclusive cache with a directory ('noninclusive_with_directory', required for non-inclusive caches with multiple upper level caches directly above them)
                              'max_requests_per_cycle': -1,                     #Maximum number of requests to accept per cycle. 0 or negative is unlimited.
                              'request_link_width': '0B',                       #Limits number of request bytes sent per cycle. Use 'B' units. '0B' is unlimited.
                              'response_link_width': '0B',                      #Limits number of response bytes sent per cycle. Use 'B' units. '0B' is unlimited.
                              'noninclusive_directory_entries': 0,              #Number of entries in the directory. Must be at least 1 if the non-inclusive directory exists.
                              'noninclusive_directory_associativity': 1,        #For a set-associative directory, number of ways.
                              'mshr_num_entries': -1,                           #Number of MSHR entries. Not valid for L1s because L1 MSHRs assumed to be sized for the CPU's load/store queue. Setting this to -1 will create a very large MSHR.
                              'tag_access_latency_cycles': None,                #Latency (in cycles) to access tag portion only of cache. Paid by misses and coherence requests that don't need data. If not specified, defaults to access_latency_cycles
                              'mshr_latency_cycles': 1,                         #Latency (in cycles) to process responses in the cache and replay requests. Paid on the return/response path for misses instead of access_latency_cycles. If not specified, simple intrapolation is used based on the cache access latency
                              'prefetch_delay_cycles': 1,                       #Delay prefetches from prefetcher by this number of cycles.
                              'max_outstanding_prefetch': None,                 #Maximum number of prefetch misses that can be outstanding, additional prefetches will be dropped/NACKed. Default is 1/2 of MSHR entries.
                              'drop_prefetch_mshr_level': None,                 #Drop/NACK prefetches if the number of in-use mshrs is greater than or equal to this number. Default is mshr_num_entries - 2.
                              'num_cache_slices': 1,                            #For a distributed, shared cache, total number of cache slices
                              'slice_id': 0,                                    #For distributed, shared caches, unique ID for this cache slice
                              'slice_allocation_policy': 'rr',                  #Policy for allocating addresses among distributed shared cache. Options: rr[round-robin]
                              'maxRequestDelay': 0,                             #Set an error timeout if memory requests take longer than this in ns (0: disable)
                              'snoop_l1_invalidations': 0,                      #Forward invalidations from L1s to processors. Options: 0[off], 1[on]
                              'debug': 0,                                       #Where to send output. Options: 0[no output], 1[stdout], 2[stderr], 3[file]
                              'debug_level': 0,                                 #Debugging level: 0 to 10. Must configure sst-core with '--enable-debug'. 1=info, 2-10=debug output
                              'debug_addr': '',                                 #Address(es) to be debugged. Leave empty for all, otherwise specify one or more, comma-separated values. Start and end string with brackets
                              'verbose': self.verbose,                          #Output verbosity for warnings/errors. 0[fatal error only], 1[warnings], 2[full state dump on fatal error]
                              #'cache_line_size': ,                             #...again...
                              'force_noncacheable_reqs': 0,                     #Used for verification purposes. All requests are considered to be 'noncacheable'. Options: 0[off], 1[on]
                              'min_packet_size': '8B',                          #Number of bytes in a request/response not including payload (e.g., addr + cmd). Specify in B.
                              'banks': 0,                                       #Number of cache banks: One access per bank per cycle. Use '0' to simulate no bank limits (only limits on bandwidth then are max_requests_per_cycle and *_link_width
                              #'network_address': ,                             #DEPRECATED
                              #'network_bw': ,                                  #MOVED
                              #'network_input_buffer_size': ,                   #MOVED
                              #'network_output_buffer_size': ,                  #MOVED
                              'prefetcher': 'NextBlockPrefetcher',              #MOVED (but we use it here regardless for later)
                              #'replacement_policy': ,                          #MOVED
                              #'noninclusive_directory_repl': ,                 #MOVED
                              #'hash_function': ,                               #MOVED
                              })
        cache = dict()
        for key in cp['DEFAULT']:
            cache[key] = cp.get(section, key)
        #
        if cache['tag_access_latency_cycles'] is None:
            cache['tag_access_latency_cycles'] = cache['access_latency_cycle']
        if cache['max_outstanding_prefetch'] is None:
            cache['max_outstanding_prefetch'] = 0.5 * cache['mshr_num_entries']
        if cache['drop_prefetch_mshr_level'] is None:
            cache['drop_prefetch_mshr_level'] = cache['mshr_num_entries'] - 2
        #
        return cache

    def getCacheCfg(self, core_id, lvl):
        return self.cache['%sCache' % lvl]


    def _parseCachePrefetcher(self, cp, section):
        if 'StridePrefetcher' in self.cache[section]['prefetcher']:
            cp['DEFAULT'] = dict({'verbose': self.verbose,                      #Controls the verbosity of the Cassini component
                                  'cache_line_size': self.cache[section]['cache_line_size'], #Size of the cache line the prefetcher is attached to
                                  'history': 16,                                #Number of entries to keep for historical comparison
                                  'reach': 2,                                   #Reach (how far forward the prefetcher should fetch lines)
                                  'detect_range': 4,                            #Range to detect addresses over in request counts
                                  'address_count': 64,                          #Number of addresses to keep in prefetch table
                                  'page_size': 4096,                            #Page size for this controller
                                  'overrun_page_boundaries': 0,                 #Allow prefetcher to run over page boundaries, 0 is no, 1 is yes
                                  })
        elif 'NextBlockPrefetcher' in self.cache[section]['prefetcher']:
            cp['DEFAULT'] = dict({'cache_line_size': self.cache[section]['cache_line_size'], #Size of the cache line the prefetcher is attached to
                                  })
        else:
            exit('ERR: unknown or missing prefetcher component')
        pre = dict()
        for key in cp['DEFAULT']:
            pre[key] = cp.get(section, key)
        #
        return pre

    def getCachePrefetcherCfg(self, core_id, lvl):
        return self.cache['%sPrefetcher' % lvl]


    def _parseMainMemory(self, cp, section, mem_comp):
        mem = dict()
        # dramsim3 for main memory module
        if 'dramsim3' == mem_comp:
            cp['DEFAULT'] = dict({'debug_level': 0,                             #Debugging level: 0 (no output) to 10 (all output). Output also requires that SST Core be compiled with '--enable-debug'
                                  'debug_mask': 0,                              #Mask on debug_level
                                  'debug_location': 0,                          #0: No debugging, 1: STDOUT, 2: STDERR, 3: FILE
                                  'max_requests_per_cycle': -1,                 #Maximum number of requests to accept each cycle. Use 0 or -1 for unlimited.
                                  'request_width': 64,                          #Maximum size, in bytes, for a request
                                  'mem_size': '2GiB',                           #[<req>]Size of memory with units (SI ok). E.g., '2GiB'.
                                  'verbose': self.verbose,                      #Sets the verbosity of the backend output
                                  'config_ini': '',                             #[<req>]Name of the DRAMSim3 Device config file
                                  'output_dir': './',                           #Name of the DRAMSim3 output directory
                                  })
            for key in cp['DEFAULT']:
                mem[key] = cp.get(section, key)
        else:
            exit('ERR: unknown or missing mainmemory component')
        #
        return mem

    def getMainMemoryCfg(self, core_id):
        return self.mem


    def _parseMemController(self, cp, section):
        mem_ctrl = dict()
        cp['DEFAULT'] = dict({'backend.mem_size': '2GiB',                       #[<req>]Size of physical memory. NEW REQUIREMENT: must include units in 'B' (SI ok). Simple fix: add 'MiB' to old value.
                              'clock': self.core['clock'],                      #[<req>]Clock frequency of controller
                              'backendConvertor': 'memHierarchy.simpleMembackendConvertor', #Backend convertor to load
                              'backend': 'memHierarchy.simpleMem',              #Backend memory model to use for timing.  Defaults to simpleMem
                              'request_width': 64,                              #Max request width to the backend
                              'trace_file': '',                                 #File name (optional) of a trace-file to generate.
                              'verbose': self.verbose,                          #Output verbosity for warnings/errors. 0[fatal error only], 1[warnings], 2[full state dump on fatal error]
                              'debug_level': 0,                                 #Debugging level: 0 to 10. Must configure sst-core with '--enable-debug'. 1=info, 2-10=debug output
                              'debug': 0,                                       #0: No debugging, 1: STDOUT, 2: STDERR, 3: FILE.
                              'debug_addr': '',                                 #Address(es) to be debugged. Leave empty for all, otherwise specify one or more, comma-separated values. Start and end string with brackets
                              'listenercount': 0,                               #Counts the number of listeners attached to this controller, these are modules for tracing or components like prefetchers
                              #'listener%': ,                                   #Loads a listener module into the controller
                              'backing': 'mmap',                                #Type of backing store to use. Options: 'none' - no backing store (only use if simulation does not require correct memory values), 'malloc', or 'mmap'
                              'backing_size_unit': '1MiB',                      #For 'malloc' backing stores, malloc granularity
                              'memory_file': 'N/A',                             #Optional backing-store file to pre-load memory, or store resulting state
                              'addr_range_start': 0,                            #Lowest address handled by this memory.
                              'addr_range_end': 2**64-1,                        #Highest address handled by this memory.
                              'interleave_size': '0B',                          #Size of interleaved chunks. E.g., to interleave 8B chunks among 3 memories, set size=8B, step=24B
                              'interleave_step': '0B',                          #Distance between interleaved chunks. E.g., to interleave 8B chunks among 3 memories, set size=8B, step=24B
                              'customCmdMemHandler': '',                        #Name of the custom command handler to load
                              })
        for key in cp['DEFAULT']:
            mem_ctrl[key] = cp.get(section, key)
        for key in ['listener%s' % c
                    for c in range(0, mem_ctrl['listenercount'])]:
            mem_ctrl[key] = cp.get(section, key)

        if 'simpleMem' in mem_ctrl['backend']:
            cp['DEFAULT'] = dict({'debug_level': 0,                             #Debugging level: 0 (no output) to 10 (all output). Output also requires that SST Core be compiled with '--enable-debug'
                                  'debug_mask': 0,                              #Mask on debug_level
                                  'debug_location': 0,                          #0: No debugging, 1: STDOUT, 2: STDERR, 3: FILE
                                  'max_requests_per_cycle': -1,                 #Maximum number of requests to accept each cycle. Use 0 or -1 for unlimited.
                                  'request_width': 64,                          #Maximum size, in bytes, for a request
                                  'mem_size': '2GiB',                           #[<req>]Size of memory with units (SI ok). E.g., '2GiB'.
                                  'access_time': '100ns',                       #Constant latency of memory operations. With units (SI ok).
                                  })
            for key in cp['DEFAULT']:
                mem_ctrl['backend_cfg'][key] = cp.get('memHierarchy.simpleMem', key)
        else:
            exit('ERR: unknown or missing backend for MemController')
        #
        return mem_ctrl

    def getMemControllerCfg(self, core_id):
        return self.mem_ctrl


    def _parseDirectoryController(self, cp, section):
        dir_ctrl = dict()
        cp['DEFAULT'] = dict({'clock': self.core['clock'],                      #Clock rate of controller.
                              'entry_cache_size': 0,                            #Size (in # of entries) the controller will cache.
                              'debug': 0,                                       #Where to send debug output. 0: No debugging, 1: STDOUT, 2: STDERR, 3: FILE.
                              'debug_level': 0,                                 #Debugging level: 0 to 10. Must configure sst-core with '--enable-debug'. 1=info, 2-10=debug output
                              'debug_addr': '',                                 #Address(es) to be debugged. Leave empty for all, otherwise specify one or more, comma-separated values. Start and end string with brackets
                              'verbose': self.verbose,                          #Output verbosity for warnings/errors. 0[fatal error only], 1[warnings], 2[full state dump on fatal error
                              'cache_line_size': 64,                            #Size of a cache line [aka cache block] in bytes.
                              'coherence_protocol': 'MESI',                     #Coherence protocol.  Supported --MESI, MSI--
                              'mshr_num_entries': -1,                           #Number of MSHRs. Set to -1 for almost unlimited number.
                              'net_memory_name': '',                            #For directories connected to a memory over the network: name of the memory this directory owns
                              'access_latency_cycles': 0,                       #Latency of directory access in cycles
                              'mshr_latency_cycles': 0,                         #Latency of mshr access in cycles
                              'max_requests_per_cycle': 0,                      #Maximum number of requests to process per cycle (0 or negative is unlimited
                              'mem_addr_start': 0,                              #Starting memory address for the chunk of memory that this directory controller addresses.
                              'addr_range_start': 0,                            #Lowest address handled by this directory.
                              'addr_range_end': 2**64-1,                        #Highest address handled by this directory.
                              'interleave_size': '0B',                          #Size of interleaved chunks. E.g., to interleave 8B chunks among 3 directories, set size=8B, step=24B
                              'interleave_step': '0B',                          #Distance between interleaved chunks. E.g., to interleave 8B chunks among 3 directories, set size=8B, step=24B
                              'node': 0,                                        #[<req>]Node number in multinode environment
                              #'network_num_vc': ,                              #DEPRECATED
                              #'network_address': ,                             #DEPRECATED
                              #'network_bw': ,                                  #MOVED
                              #'network_input_buffer_size': ,                   #MOVED
                              #'network_output_buffer_size': ,                  #MOVED
                              })
        for key in cp['DEFAULT']:
            dir_ctrl[key] = cp.get(section, key)
        #
        return dir_ctrl

    def getDirectoryControllerCfg(self, core_id):
        return self.dir_ctrl


    def _parseNetworkOnChip(self, cp, section, noc_comp):
        noc = dict()
        # ring network on chip
        if 'hr_router' == noc_comp:
            cp['DEFAULT'] = dict({'id': 0,                                      #ID of the router.
                                  'num_ports': 3,                               #Number of ports that the router has
                                  'topology': 'torus',                          #Name of the topology subcomponent that should be loaded to control routing.
                                  'xbar_arb': 'merlin.xbar_arb_lru',            #Arbitration unit to be used for crossbar.
                                  'link_bw': '64GiB/s',                         #Bandwidth of the links specified in either b/s or B/s (can include SI prefix).
                                  'flit_size': '8B',                            #Flit size specified in either b or B (can include SI prefix).
                                  'xbar_bw': '1TiB/s',                         #Bandwidth of the crossbar specified in either b/s or B/s (can include SI prefix).
                                  'input_latency': 0,                           #Latency of packets entering switch into input buffers.  Specified in s (can include SI prefix).
                                  'output_latency': 0,                          #Latency of packets exiting switch from output buffers.  Specified in s (can include SI prefix).
                                  'input_buf_size': 0,                          #Size of input buffers specified in b or B (can include SI prefix).
                                  'output_buf_size': 0,                         #Size of output buffers specified in b or B (can include SI prefix).
                                  'network_inspectors': '',                     #Comma separated list of network inspectors to put on output ports.
                                  'oql_track_port': False,                      #Set to true to track output queue length for an entire port.  False tracks per VC.
                                  'oql_track_remote': False,                    #Set to true to track output queue length including remote input queue.  False tracks only local queue.
                                  'num_vns': 2,                                 #Number of VNs.
                                  'vn_remap': '',                               #Array that specifies the vn remapping for each node in the systsm.
                                  'vn_remap_shm': '',                           #Name of shared memory region for vn remapping.  If empty, no remapping is done
                                  'debug': self.verbose,                        #Turn on debugging for router. Set to 1 for on, 0 for off.
                                  })
            for key in cp['DEFAULT']:
                noc[key] = cp.get(section, key)
            #
            if 'torus' in noc['topology']:
                cp['DEFAULT'] = dict({'shape': self.socket['num_cmg'],          #Shape of the torus specified as the number of routers in each dimension, where each dimension is separated by an x.  For example, 4x4x2x2.  Any number of dimensions is supported.
                                      'width': 1,                               #Number of links between routers in each dimension, specified in same manner as for shape.  For example, 2x2x1 denotes 2 links in the x and y dimensions and one in the z dimension.
                                      'local_ports': 3,                         #Number of endpoints attached to each router.
                                      })
                for key in cp['DEFAULT']:
                    noc['topology_cfg'][key] = cp.get('merlin.torus', key)
        #
        # mesh network on chip
        elif 'noc_mesh' == noc_comp:
            cp['DEFAULT'] = dict({'local_ports': 1,                             #Number of ports that are dedicated to endpoints.
                                  'link_bw': '64GB/s',                          #[<req>]Bandwidth of the links specified in either b/s or B/s (can include SI prefix).
                                  'flit_size': '8B',                            #[<req>]Flit size specified in either b or B (can include SI prefix).
                                  'input_buf_size': None,                       #[<req>]Size of input buffers in either b or B (can use SI prefix).  Default is 2*flit_size.
                                  'port_priority_equal': False,                 #Set to true to have all port have equal priority (usually endpoint ports have higher priority).
                                  'route_y_first': False,                       #Set to true to rout Y-dimension first.
                                  'use_dense_map': False,                       #Set to true to have a dense network id map instead of the sparse map normally used.
                                  })
            for key in cp['DEFAULT']:
                noc[key] = cp.get(section, key)
            #
            if noc['input_buf_size'] is None:
                noc['input_buf_size'] = 2 * noc['flit_size']
        #
        else:
            exit('ERR: unknown or missing noc component')
        #
        return noc

    def getNetworkOnChipCfg(self, core_id, router_id):
        noc = self.noc
        noc['id'] = router_id
        return noc


    def _parseWorkload(self, cp, section, wl_type):
        wl = dict()
        # Miranda
        if 'miranda' == wl_type:
            section = 'Miranda'
            cp['DEFAULT'] = dict({'max_reqs_cycle': 2,                          #Maximum number of requests the CPU can issue per cycle (this is for all reads and writes)
                                  'max_reorder_lookups': 16,                    #Maximum number of operations the CPU is allowed to lookup for memory reorder
                                  'cache_line_size': 64,                        #The size of the cache line that this prefetcher is attached to, default is 64-bytes
                                  'maxmemreqpending': 16,                       #Set the maximum number of requests allowed to be pending
                                  'verbose': self.verbose,                      #Sets the verbosity of output produced by the CPU
                                  'generator': 'miranda.SingleStreamGenerator', #The generator to be loaded for address creation
                                  'clock': self.core['clock'],                  #Clock for the base CPU
                                  'memoryinterface': 'memHierarchy.memInterface',   #Sets the memory interface module to use
                                  'pagecount': 4194304,                         #Sets the number of pages the system can allocate
                                  'pagesize': 4096,                             #Sets the size of the page in the system, MUST be a multiple of cache_line_size
                                  'pagemap': 'LINEAR',                          #Mapping scheme, string set to LINEAR or RANDOMIZED, default is LINEAR (virtual==physical), RANDOMIZED randomly shuffles virtual to physical map.
                                  'pagemapname': 'miranda',                     #Name of the shared memory region to keep page mapping in
                                  })
            for key in cp['DEFAULT']:
                wl[key] = cp.get(section, key)

            if 'STREAMBenchGenerator' == wl['generator']:
                cp['DEFAULT'] = dict({'verbose': verbose,                  #Sets the verbosity output of the generator
                                      'n': 10000,                               #Sets the number of elements in the STREAM arrays
                                      'n_per_call': 1,                          #Sets the number of iterations to generate per call to the generation function
                                      'operandwidth': 8,                        #Sets the length of the request, default=8 (i.e. one double
                                      'start_a': 0,                             #Sets the start address of the array a
                                      'start_b': 1024,                          #Sets the start address of the array b
                                      'start_c': 2048,                          #Sets the start address of the array c
                                      'total_streamN': -1,
                                      })
                wl['generator_cfg'] = dict()
                for key in cp['DEFAULT']:
                    wl['generator_cfg'][key] = cp.get('miranda.STREAMBenchGenerator', key)
            else:
                exit('ERR: unknown or missing generator for miranda')
        #
        # Ariel
        elif 'ariel' == wl_type:
            section = 'Ariel'
            cp['DEFAULT'] = dict({'verbose': self.verbose,                      #Verbosity for debugging. Increased numbers for increased verbosity.
                                  'profilefunctions': 0,                        #Profile functions for Ariel execution, 0 = none, >0 = enable
                                  'corecount': 0,                               #Number of CPU cores to emulate
                                  'checkaddresses': 0,                          #Verify that addresses are valid with respect to cache lines
                                  'maxissuepercycle': self.core['max_reqs_cycle'],  #Maximum number of requests to issue per cycle, per core
                                  'maxcorequeue': 64,                           #Maximum queue depth per core
                                  'maxtranscore': 16,                           #Maximum number of pending transactions
                                  'pipetimeout': 10,                            #Read timeout between Ariel and traced application
                                  'cachelinesize': 64,                          #Line size of the attached caching structure
                                  'arieltool': '',                              #Path to the Ariel PIN-tool shared library
                                  'launcher': 'PINTOOL_EXECUTABLE',             #Specify the launcher to be used for instrumentation, default is path to PIN
                                  'executable': '',                             #Executable to trace
                                  'launchparamcount': 0,                        #Number of parameters supplied for the launch tool
                                  #'launchparam%': '',                          #Set the parameter to the launcher
                                  'envparamcount': -1,                          #Number of environment parameters to supply to the Ariel executable, default=-1 (use SST environment)
                                  #'envparamname%': '',                         #Sets the environment parameter name
                                  #'envparamval%': '',                          #Sets the environment parameter value
                                  'appargcount': 0,                             #Number of arguments to the traced executable
                                  #'apparg%': '',                               #Arguments for the traced executable
                                  'arielmode': 2,                               #Tool interception mode, set to 1 to trace entire program (default), set to 0 to delay tracing until ariel_enable() call., set to 2 to attempt auto-detect
                                  'arielinterceptcalls': 0,                     #Toggle intercepting library calls
                                  'arielstack': 0,                              #Dump stack on malloc calls (also requires enabling arielinterceptcalls). May increase overhead due to keeping a shadow stack.
                                  'mallocmapfile': '',                          #File with valid 'ariel_malloc_flag' ids
                                  'tracePrefix': '',                            #Prefix when tracing is enable
                                  'clock': self.core['clock'],                  #Clock rate at which events are generated and processed
                                  'tracegen': '',                               #Select the trace generator for Ariel (which records traced memory operations
                                  'memmgr': 'ariel.MemoryManagerSimple',        #Memory manager to use for address translation) [ariel.MemoryManagerSimple
                                  'writepayloadtrace': 0,                       #Trace write payloads and put real memory contents into the memory system
                                  'instrument_instructions': 1,                 #turn on or off instruction instrumentation in fesimple
                                  'gpu_enabled': 0,                             #If enabled, gpu links will be set up
                                  })
            for key in cp['DEFAULT']:
                wl[key] = cp.get(section, key)
            for key in ['launchparam%s' % c
                        for c in range(0, wl['launchparamcount'])]:
                wl[key] = cp.get(section, key)
            for key in ['envparamname%s' % c
                        for c in range(0, wl['envparamcount'])]:
                wl[key] = cp.get(section, key)
            for key in ['envparamval%s' % c
                        for c in range(0, wl['envparamcount'])]:
                wl[key] = cp.get(section, key)
            for key in ['apparg%s' % c
                        for c in range(0, wl['appargcount'])]:
                wl[key] = cp.get(section, key)
            #
            if 'MemoryManagerSimple' == wl['memmgr']:
                cp['DEFAULT'] = dict({'verbose': self.verbose,                  #Verbosity for debugging. Increased numbers for increased verbosity.
                                      'vtop_translate': 'yes',                  #Set to yes to perform virt-phys translation (TLB) or no to disable
                                      'pagemappolicy': 'LINEAR',                #Select the page mapping policy for Ariel [LINEAR|RANDOMIZED]
                                      'translatecacheentries': 4096,            #Keep a translation cache of this many entries to improve emulated core performance
                                      'pagesize0': 4096,                        #Page size
                                      'pagecount0': 131072,                     #Page count
                                      'page_populate_0': '',                    #Pre-populate/partially pre-populate the page table, this is the file to read in.
                                      })
                wl['memmgr_cfg'] = dict()
                for key in cp['DEFAULT']:
                    wl['memmgr_cfg'][key] = cp.get('ariel.MemoryManagerSimple', key)
            else:
                exit('ERR: unknown or missing memmgr for ariel')
            #
            # The -ifeellucky option is required for Pin 2.14 to work with modern processors. May be removed with newer Pin versions.
            #wl['launchparamcount'] = 1
            #wl['launchparam0'] = '-ifeellucky'
            if wl['corecount'] < 1:
                wl['corecount'] = self.total_cores
            if path.isfile(wl['executable']):
                if 'OMP_EXE' in environ and path.isfile(environ['OMP_EXE']):
                    wl['executable'] = environ['OMP_EXE']
                else:
                    raise Exception('ERR: no Ariel executable specified')
        #
        else:
            exit('ERR: unknown or missing workload component')
        #
        return wl

    def _getMirandaSTREAMBenchGeneratorCfg(self, core_id, wl):
        sN = wl['generator_cfg']['total_streamN']
        opW = wl['generator_cfg']['operandwidth']
        tc = self.total_cores
        return dict({'verbose': self.verbose,
                     'n': sN // tc,
                     'n_per_call': 1,
                     'operandwidth': opW,
                     'start_a': ((sN * opW) // tc) * core_id,
                     'start_b': ((sN * opW) // tc) * core_id + (sN * opW),
                     'start_c': ((sN * opW) // tc) * core_id + (2 * sN * opW),
                     })

    def getWorkload(self, core_id):
        wl = self.workload
        if 'miranda' == self.comp['workload']:
            if wl['generator_cfg']['total_streamN'] > -1:
                wl['generator_cfg'] = self._getMirandaSTREAMBenchGeneratorCfg(core_id, wl)
        #
        return wl


# support fn
def connect(name, c0, port0, c1, port1, latency):
    link = sst.Link(name)
    link.connect( (c0, port0, latency), (c1, port1, latency) )
    return link


# Parse commandline arguments
parser = ArgumentParser()
parser.add_argument("-c", "--config", required=True,
                    help="specify configuration file [eg. ./util/a64fx.cfg]")
parser.add_argument("-v", "--verbose", action="store_true",
                    help="increase verbosity of output")
parser.add_argument("-s", "--statfile", default="./stats.csv",
                    help="statistics file")
parser.add_argument("-l", "--statlevel", type=int, default=16,
                    help="statistics level")
args = parser.parse_args()

verbose = args.verbose
cfg_file = args.config
stat_file = args.statfile
stat_level = args.statlevel

router_map = dict()

print('JJ: parsing basic model info')
config = SimCfgParser(cfg_file, verbose=verbose)

print('JJ: setting up model workload')
cpu = dict()
if 'ariel' in config.workload:
    cpu[0] = dict({'core': sst.Component('ariel0', 'ariel.ariel'),
                   'cache_link': 'cache_link_%d' % 0,
                   })
    cpu[0].addParams(config.getWorkload(0))
    for cmg_id in range(0, config.socket['num_cmg']):
        for core_id in range(0, config.cmg['num_cores']):
            abs_core_id = cmg_id * config.cmg['num_cores'] + core_id
            if abs_core_id > 0:
                cpu[abs_core_id] = dict({'core': cpu[0]['core'],
                                         'cache_link': 'cache_link_%d' % abs_core_id,
                                         })
elif 'miranda' in config.workload:
    for cmg_id in range(0, config.socket['num_cmg']):
        for core_id in range(0, config.cmg['num_cores']):
            abs_core_id = cmg_id * config.cmg['num_cores'] + core_id
            cpu[abs_core_id] = dict({'core': sst.Component('miranda%d' % (abs_core_id),
                                                           'miranda.BaseCPU'),
                                     'cache_link': 'cache_link',
                                     })
            cpu[abs_core_id].addParams(config.getWorkload(abs_core_id))

print('JJ: configuring ring network between CMGs')
if 'hr_router' in config.comp['networkonchip']:
    for ring_stop_id in range(0, config.socket['num_cmg']):
        ring_stop_router = sst.Component('rsr%d' % ring_stop_id, 'merlin.hr_router')
        ring_stop_router.addParams(config.getNetworkOnChipCfg(-1, ring_stop_id))
        ring_stop_router.setSubComponent('topology', 'merlin.torus')

        router_map['rsr%d' % ring_stop_id] = ring_stop_router

    for ring_stop_id in range(0, config.socket['num_cmg']):
        rsr_link = sst.Link('L:rsr%d>rsr%d' % (ring_stop_id,
                                               ring_stop_id % (config.socket['num_cmg']-1)))
        rsr_link.connect( (router_map['rsr%d' % ring_stop_id],
                           "port0",
                           config.noc['output_latency']),
                          (router_map['rsr%d' % (ring_stop_id % (config.socket['num_cmg']-1))],
                           "port1",
                           config.noc['input_latency']) )

print('JJ: connecting cores and caches')
for cmg_id in range(0, config.socket['num_cmg']):

    l2 = sst.Component('CMG%dL2' % cmg_id, 'memHierarchy.Cache')
    params = config.getCacheCfg(abs_core_id, 'L2')
    prefetcher = params.pop('prefetcher')
    l2.addParams(params)
    l2prefetch = l2.setSubComponent('prefetcher', 'cassini.%s' % prefetcher)
    l2prefetch.addParams(config.getCachePrefetcherCfg(abs_core_id, 'L2'))

    l1l2bus = sst.Component('CMG%dL1L2Bus' % cmg_id, 'memHierarchy.Bus')
    l1l2bus.addParams({'bus_frequency': config.core['clock']})

    connect('L:CMG%dL1L2Bus>L2' % cmg_id,
            l1l2bus, 'low_network_0',
            l2, 'high_network_0',
            '50ps').setNoCut()

    for core_id in range(0, config.cmg['num_cores']):
        abs_core_id = cmg_id * config.cmg['num_cores'] + core_id

        l1 = sst.Component('C%dL1D' % abs_core_id, 'memHierarchy.Cache')
        params = config.getCacheCfg(abs_core_id, 'L1')
        prefetcher = params.pop('prefetcher')
        l1.addParams(params)
        l1prefetch = l1.setSubComponent('prefetcher', 'cassini.%s' % prefetcher)
        l1prefetch.addParams(config.getCachePrefetcherCfg(abs_core_id, 'L1'))

        connect('L:C%d>L1D' % abs_core_id,
                cpu[abs_core_id]['core'], cpu[abs_core_id]['cache_link'],
                l1, 'high_network_0',
                '50ps').setNoCut()

        connect('L:C%dL1D>L2' % abs_core_id,
                l1, 'low_network_0',
                l1l2bus, 'high_network_%d' % core_id,
                '50ps').setNoCut()

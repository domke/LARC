#!/bin/bash
set -x

SELF="$(readlink -f "${BASH_SOURCE[0]}")"

if [ -z "$CRTOOLS_SCRIPT_ACTION" ] && [ -z "$1" ]; then

	for Gem5PID in $(pidof gem5.fast); do
		unset APPWORKDIR; unset APPROOT; unset RSYNCFILTER; unset LOG; unset RLOG; unset CRIU_DUMP_DIR
		unset criuTMPDMP; unset criuGemPID; unset criuGemUSR; unset criuGemGRP; unset criuAPPDIR; unset criuRSYNCFILTER
		# get env stuff
		CTIME="$(date +%s.%N)"
		export criuTMPDMP="/tmp/${CTIME}"
		mkdir -p "${criuTMPDMP}"
		cp /tmp/gem5run_${Gem5PID} "${criuTMPDMP}"
		source /tmp/${CTIME}/gem5run_${Gem5PID}
		cd "${APPWORKDIR}"
		export criuGemPID="${Gem5PID}"
		export criuGemUSR="$(ps -o uname= -p ${Gem5PID})"; echo "export Gem5USR=\"${criuGemUSR}\"" >> "${criuTMPDMP}/gem5run_${Gem5PID}"
		export criuGemGRP="$(ps -o rgroup= -p ${Gem5PID})"; chown -R "${criuGemUSR}:${criuGemGRP}" "${criuTMPDMP}"
		export criuAPPDIR="${APPROOT}"
		if [ -n "${RSYNCFILTER}" ]; then export criuRSYNCFILTER="--filter=\". ${RSYNCFILTER}\""; fi
		#export criuGEMLOG="${LOG}"
		#export criuGEMSTA="${LOG}_stat"
		criu dump --tree ${Gem5PID} -vvv -o dump.log --shell-job \
			--leave-running --track-mem --link-remap \
			--file-locks  --images-dir "${criuTMPDMP}" \
			--action-script "${SELF}"
		#XXX: cannot give param, but they are in the env: -action-script "${SELF} ${Gem5PID} /tmp/${CTIME} ${APPROOT} ${LOG} ${LOG}_stat"
		chown -R "${criuGemUSR}:${criuGemGRP}" "${criuTMPDMP}"
		sudo -H -u "${criuGemUSR}" mkdir -p "${CRIU_DUMP_DIR}"
		sudo -H -u "${criuGemUSR}" mv "${criuTMPDMP}" "${CRIU_DUMP_DIR}"
	done

elif [ -z "$CRTOOLS_SCRIPT_ACTION" ] && [ -n "$1" ] && [ "$1" = "restart" ]; then

	if ! timeout -k 3s 2s sudo -v >/dev/null; then echo "ERR: need to run sudo inside, please run \`sudo -v\` and provide pw before running this command or allow user to run \`sudo criu restore ...\`"; exit 1; fi
	CHECKPDIR="$2"
	CTIME="$(basename ${CHECKPDIR})"
	if which rsync >/dev/null && [ -n "${CHECKPDIR}" ] && [ -d "${CHECKPDIR}" ] && [ $(find "${CHECKPDIR}" -mindepth 1 -maxdepth 1 -type f -name 'gem5run_[0-9]*' | wc -l) -ge 1 ]; then
		Gem5PIDFile="$(find "${CHECKPDIR}" -mindepth 1 -maxdepth 1 -type f -name 'gem5run_[0-9]*')"
		source "${Gem5PIDFile}"
		if [ "${Gem5USR}" != "$(logname)" ]; then echo "ERR: gem user not matching restart user"; exit 1; fi
		#criuAPPDIR="${APPROOT}"
		#criuGEMLOG="${LOG}"
		#criuGEMSTA="${LOG}_stat"
		mv "${APPROOT}" "${APPROOT}.restartbak.${CTIME}"
		#mv "${criuGEMLOG}" "${criuGEMLOG}.restartbak.${CTIME}"
		#mv "${criuGEMSTA}" "${criuGEMSTA}.restartbak.${CTIME}"
		rsync -aHAX "${Gem5PIDFile}" /tmp
		rsync -aHAX "$(readlink -f "${CHECKPDIR}")/"* "$(dirname ${APPROOT})"
		#rsync -aHAX "${CHECKPDIR}/$(basename ${APPROOT})" "$(dirname ${APPROOT})"
		#rsync -aHAX "${CHECKPDIR}/$(basename ${criuGEMLOG})" "$(dirname ${criuGEMLOG})"
		#rsync -aHAX "${CHECKPDIR}/$(basename ${criuGEMSTA})" "$(dirname ${criuGEMLOG})"
		cd "${APPWORKDIR}"
		sudo criu restore -vvv --shell-job --link-remap --file-locks \
			--images-dir "$(dirname ${APPROOT})" #--action-script "${SELF}"
		rsync -aHAX "${LOG}"* "$(dirname ${RLOG})"
	else
		echo "ERR: missing restart directory"; exit 1
	fi

elif [ "$CRTOOLS_SCRIPT_ACTION" = "pre-dump" ]; then
	sleep 0
elif [ "$CRTOOLS_SCRIPT_ACTION" = "post-dump" ]; then

	if which rsync >/dev/null && [ -n "${criuGemUSR}" ] && [ -n "${criuTMPDMP}" ] && [ -n "${criuAPPDIR}" ]; then  # && [ -n "${criuGEMLOG}" ] && [ -n "${criuGEMSTA}" ]; then
		eval sudo -H -u "${criuGemUSR}" rsync -aHAX "${criuRSYNCFILTER}" "${criuAPPDIR}" "${criuTMPDMP}"
		#sudo -H -u "${criuGemUSR}" rsync -aHAX "${criuGEMLOG}" "${criuTMPDMP}"
		#sudo -H -u "${criuGemUSR}" rsync -aHAX "${criuGEMSTA}" "${criuTMPDMP}"
	fi

elif [ "$CRTOOLS_SCRIPT_ACTION" = "pre-restore" ]; then
	sleep 0
elif [ "$CRTOOLS_SCRIPT_ACTION" = "post-restore" ]; then
	sleep 0
elif [ "$CRTOOLS_SCRIPT_ACTION" = "network-lock" ]; then
	sleep 0
elif [ "$CRTOOLS_SCRIPT_ACTION" = "network-unlock" ]; then
	sleep 0
elif [ "$CRTOOLS_SCRIPT_ACTION" = "setup-namespaces" ]; then
	sleep 0
fi

exit 0

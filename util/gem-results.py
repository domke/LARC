#!/usr/bin/python3

# author: Emil Vatai
# date: 2022-03-24
#
# A script which collects gem5 results into a pandas data frame. I
# assume then it will be modified to create plots etc.

import argparse
from pathlib import Path

import pandas as pd

CONF_GLOB = "conf*.log"
WALLTIME_STR = "Walltime of the main kernel:"
SIM_SECONDS_STR = "sim_seconds"
WALLTIME_KEY = "walltime"
BINARY_KEY = "binary"
RATIO_KEY = "system.l2.overall_hit_miss_ratio"
HITS_KEY = "L2hits"
MISSES_KEY = "L2misses"
CONF_12CORE_LIST = [
    "03.kernel_June1_Single_Tune_Full_pr64",
    "04.kernel_July_Single_c_m1_rvl",
    "05.PairList_June_Single_simu2_c_rvl_align",
    "06.PairList_July_Single_tune_20170901",
    "19.Adventure.region2.tune161011.armtest-M24.170727",
]
RUN_SELECTOR = {
    "08.NICAM.divdamp.r4_collapsed.tune01.170420": "kiev1",
}


def get_walltime(log_file, match_str=WALLTIME_STR):
    """Get walltime from `log_file`.

    A list of (key, values) is returned.

    """
    with open(log_file, errors="ignore") as f:
        for line in f:
            if match_str in line:
                wtime = line.split(match_str)[1].split()[0]
                walltime = float(wtime.replace("D", "e"))
                return [(WALLTIME_KEY, walltime)]
            # add this because of fs2020/21 benchmark
            if "fs2020/21" in str(log_file) and line.startswith("Walltime of the"):
                wtime = line.split(":")[1].split()[0]
                walltime = float(wtime.replace("D", "e"))
                return [(WALLTIME_KEY, walltime)]

    return []


def get_cache_ratio(stats_txt):
    """Get ratio of hits/misses from `stats.txt` file.

    A list of (key, values) is returned.
    """
    hits_total = 0
    misses_total = 0
    with open(stats_txt) as f:
        for line in f:
            if "system.l2s" in line:
                if "overall_hits::total" in line:
                    fields = line.split()
                    value = int(fields[1])
                    # conf_entry[fields[0]] = value
                    hits_total += value
                if "overall_misses::total" in line:
                    fields = line.split()
                    value = int(fields[1])
                    # conf_entry[fields[0]] = value
                    misses_total += value
    return [(HITS_KEY, hits_total), (MISSES_KEY, misses_total)]


def get_normalised_conf_name(log_file):
    """Fix inconsistent filenames.

    Some of the runs use `conf-1.log` and some `conf1.log`.  This
    delets the dash in `conf-1.log`.
    """
    return log_file.name.replace("-", "")


def get_bin_name(curdir):
    """Get (the base of) the `binary` column in the CSV."""
    lst = str(curdir).split("/")
    begin = lst.index("log") + 3  # = log/host/gem5run
    bin_name = "/".join(lst[begin:])
    return bin_name


def get_conf_entry(log_file):
    """Get the results for a single configuration."""
    # skip conf4
    if "4" in log_file.name:
        return []
    log_stat = Path(f"{log_file}_stat")

    conf_entry = {}
    conf_entry.update(get_walltime(log_file))

    stats_txt = log_stat / "stats.txt"
    if stats_txt.exists():
        conf_entry.update(get_cache_ratio(stats_txt))
    if conf_entry:
        return [(get_normalised_conf_name(log_file), conf_entry)]
    else:
        return []


def sum_spec_entry(configs):
    """Get the entry for a spec binary (time summed, hit/miss averaged).

    Returns a dictionary similar to: {"conf1": {key: val, ...}, ...}.

    """
    data = {}
    for log_file in configs:
        conf_name = get_normalised_conf_name(log_file)
        data[conf_name] = {}
        subdirs = Path(f"{log_file}_stat").glob("*")
        for workload in subdirs:
            stats_txt = workload / "stats.txt"
            if stats_txt.is_file():
                entry = {}
                entry.update(get_cache_ratio(stats_txt))
                entry.update(get_walltime(stats_txt, SIM_SECONDS_STR))
                data[conf_name][workload.name] = entry
    entry = {}
    for conf in data:
        entry[conf] = {}
        workloads = data[conf].values()
        for workload in workloads:
            for k in workload:
                if k in entry[conf]:
                    entry[conf][k] += workload[k]
                else:
                    entry[conf][k] = workload[k]
        if RATIO_KEY in entry[conf]:
            entry[conf][RATIO_KEY] /= float(len(workloads))
        if entry[conf] == {}:
            del entry[conf]
    return entry


def get_entries(curdir):
    """Get the data for a single binary."""
    configs = list(curdir.glob(CONF_GLOB))
    if str(curdir.name) in CONF_12CORE_LIST:
        configs = [c for c in configs if "-" in c.name]
    if str(curdir.name) in RUN_SELECTOR:
        host = RUN_SELECTOR[curdir.name]
        configs = [c for c in configs if host in str(c)]
    if configs:
        if curdir.parent.name.startswith("spec"):
            result = sum_spec_entry(configs)
        else:
            result = {}
            for log_file in configs:
                result.update(get_conf_entry(log_file))
        if result:
            bin_name = get_bin_name(curdir)
            results = [(bin_name, result)]
            return results
    else:
        results = []
        for binary in curdir.glob("*"):
            results += get_entries(binary)
        return results
    return []


def get_data_as_df(path):
    """Combine all data into a single data frame."""
    assert path.name == "log", "Path should point to log directory"
    df = {}
    for host in path.glob("*"):
        gem5run_dir = host / "gem5run"
        for benchmark in gem5run_dir.glob("*"):
            entries = get_entries(benchmark)
            if entries:
                for binary, entry in entries:
                    if binary in df:
                        df[binary].update(entry)
                    else:
                        df[binary] = entry
    df = pd.json_normalize([{BINARY_KEY: k, **v} for k, v in df.items()])
    df = df.sort_index(axis=1)
    df = df.sort_values(by=[BINARY_KEY], ignore_index=True)
    return df


def main():
    """Entry point of the program."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--log-dir",
        default="~/tmp/sc22/log",
        help="A path which ends with `.../log` and has the data",
    )
    parser.add_argument(
        "--out-file",
        default="out.csv",
        help="The name of the CSV file which will contain the results",
    )
    args = parser.parse_args()

    path = Path(args.log_dir).expanduser()
    df = get_data_as_df(path)
    print(df.shape)
    df.to_csv(args.out_file)
    # df["walltime"]].to_csv("walltime.csv")
    for col in df.columns:
        print(col)


if __name__ == "__main__":
    main()

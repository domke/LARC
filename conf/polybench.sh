#!/bin/bash

#Pre-defined Problem Sizes; PolyBench/C 4.0 comes with 5 pre-defined problem sizes.
#The problem sizes are primarily derived from thememory requirements such that
#different levels of the memory hierarchy are exercised.
#- MINI: Less than 16KB of memory.  The problem may fit within the L1 (last level) cache.
#- SMALL: Around 128KB of memory.  The problem should not fit within the L1 cache, but may fit L2.
#- MEDIUM: Around 1MB of memory.  The problem should not fit within the L2 cache, but may fit L3.
#- LARGE: Around 25MB of memory.  The problem should not fit within the L3 cache.
#- EXTRALARGE: Around 120MB of memory.

export APPDIR="./polybench"
export BINARYS="datamining/correlation/correlation|EXTRALARGE
datamining/covariance/covariance|EXTRALARGE
linear-algebra/blas/gemm/gemm|EXTRALARGE
linear-algebra/blas/gemver/gemver|EXTRALARGE
linear-algebra/blas/gesummv/gesummv|EXTRALARGE
linear-algebra/blas/symm/symm|EXTRALARGE
linear-algebra/blas/syr2k/syr2k|EXTRALARGE
linear-algebra/blas/syrk/syrk|EXTRALARGE
linear-algebra/blas/trmm/trmm|EXTRALARGE
linear-algebra/kernels/2mm/2mm|EXTRALARGE
linear-algebra/kernels/3mm/3mm|EXTRALARGE
linear-algebra/kernels/atax/atax|EXTRALARGE
linear-algebra/kernels/bicg/bicg|EXTRALARGE
linear-algebra/kernels/doitgen/doitgen|EXTRALARGE
linear-algebra/kernels/mvt/mvt|EXTRALARGE
linear-algebra/solvers/cholesky/cholesky|EXTRALARGE
linear-algebra/solvers/durbin/durbin|EXTRALARGE
linear-algebra/solvers/gramschmidt/gramschmidt|EXTRALARGE
linear-algebra/solvers/lu/lu|EXTRALARGE
linear-algebra/solvers/ludcmp/ludcmp|EXTRALARGE
linear-algebra/solvers/trisolv/trisolv|EXTRALARGE
medley/deriche/deriche|EXTRALARGE
medley/floyd-warshall/floyd-warshall|EXTRALARGE
medley/nussinov/nussinov|EXTRALARGE
stencils/adi/adi|EXTRALARGE
stencils/fdtd-2d/fdtd-2d|EXTRALARGE
stencils/heat-3d/heat-3d|EXTRALARGE
stencils/jacobi-1d/jacobi-1d|EXTRALARGE
stencils/jacobi-2d/jacobi-2d|EXTRALARGE
stencils/seidel-2d/seidel-2d|EXTRALARGE"
export INPUT=""
export NumRunsTEST=3
export NumRunsBEST=10
export MAXTIME="20m"
export RUNSDE="yes"
export RUNPCM="no"
export RUNVTUNE="no"
export TESTCONF="1|1"
export BESTCONF="1|1"
export GEM5CONF="1|1"
export NumRunsGEM5=1

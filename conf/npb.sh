#!/bin/bash

export APPDIR="./NPB"
export BINARYS="NPB3.4-MPI/bin/bt|B
NPB3.4-MPI/bin/cg|B
NPB3.4-MPI/bin/dt|B|WH
NPB3.4-MPI/bin/ep|B
NPB3.4-MPI/bin/ft|B
NPB3.4-MPI/bin/is|B
NPB3.4-MPI/bin/lu|B
NPB3.4-MPI/bin/mg|B
NPB3.4-MPI/bin/sp|B
NPB3.4-OMP/bin/bt|B
NPB3.4-OMP/bin/sp|B
NPB3.4-OMP/bin/lu|B
NPB3.4-OMP/bin/ft|B
NPB3.4-OMP/bin/cg|B
NPB3.4-OMP/bin/mg|B
NPB3.4-OMP/bin/ep|B
NPB3.4-OMP/bin/ua|B
NPB3.4-OMP/bin/is|B
NPB3.4-OMP/bin/dc|B
NPB3.4-OMP/bin/ua|A
NPB3.4-OMP/bin/dc|A"
export INPUT=""
export NumRunsTEST=3
export NumRunsBEST=10
export MAXTIME="20m"
export RUNSDE="yes"
export RUNPCM="no"
export RUNVTUNE="no"

if [ -n "${XEONHOST}" ]; then
	export TESTCONF="1|6 1|12 1|24 1|32 1|48 1|96
			 4|1
			 6|1
			 8|1
			 9|1
			 12|1
			 16|1
			 24|1
			 25|1
			 32|1
			 36|1
			 49|1
			 48|1
			 96|1"
	export BESTCONF="1|1"
	export SCALCONF="" #"1|1024 1024|1"
	export BBINARYS="NPB3.4-MPI/bin/bt|B|36|1
			 NPB3.4-MPI/bin/cg|B|32|1
			 NPB3.4-MPI/bin/dt|B|49|1|WH
			 NPB3.4-MPI/bin/ep|B|48|1
			 NPB3.4-MPI/bin/ft|B|32|1
			 NPB3.4-MPI/bin/is|B|32|1
			 NPB3.4-MPI/bin/lu|B|24|1
			 NPB3.4-MPI/bin/mg|B|32|1
			 NPB3.4-MPI/bin/sp|B|25|1
			 NPB3.4-OMP/bin/bt|B|1|48
			 NPB3.4-OMP/bin/sp|B|1|24
			 NPB3.4-OMP/bin/lu|B|1|24
			 NPB3.4-OMP/bin/ft|B|1|48
			 NPB3.4-OMP/bin/cg|B|1|48
			 NPB3.4-OMP/bin/mg|B|1|48
			 NPB3.4-OMP/bin/ep|B|1|48
			 NPB3.4-OMP/bin/ua|B|1|48
			 NPB3.4-OMP/bin/is|B|1|48
			 NPB3.4-OMP/bin/dc|B|1|32"
elif [ -n "${IKNLHOST}" ]; then
	export TESTCONF=""
	export BESTCONF=""
elif [ -n "${IKNMHOST}" ]; then
	export TESTCONF=""
	export BESTCONF=""
elif [ -n "${FUJIHOST}" ] || [ -n "${RFX7HOST}" ]; then
	export TESTCONF="1|6 1|12 1|24 1|32 1|48
			 6|1
			 8|1
			 9|1
			 12|1
			 16|1
			 24|1
			 25|1
			 32|1
			 36|1
			 48|1"
	#XXX: many of the MPI cases didnt compile thanks to FJ's MPI
	export BBINARYS="NPB3.4-MPI/bin/dt|B|48|1|WH
			 NPB3.4-MPI/bin/is|B|32|1
			 NPB3.4-OMP/bin/bt|B|1|24
			 NPB3.4-OMP/bin/sp|B|1|48
			 NPB3.4-OMP/bin/lu|B|1|12
			 NPB3.4-OMP/bin/ft|B|1|24
			 NPB3.4-OMP/bin/cg|B|1|48
			 NPB3.4-OMP/bin/mg|B|1|12
			 NPB3.4-OMP/bin/ep|B|1|48
			 NPB3.4-OMP/bin/ua|B|1|12
			 NPB3.4-OMP/bin/is|B|1|12
			 NPB3.4-OMP/bin/dc|B|1|24"
	export BESTCONF="1|1" #encoded in binary name
elif [ -n "${MILXHOST}" ]; then
	export TESTCONF="1|8 1|16 1|24 1|32 1|48 1|96 1|128 1|160 1|192 1|256
			 8|1
			 9|1
			 16|1
			 24|1
			 25|1
			 32|1
			 36|1
			 49|1
			 48|1
			 96|1
			 121|1
			 128|1"
	export BESTCONF=""
elif [ -n "${GEM5HOST}" ]; then
	export GEM5CONF="1|48"
	export NumRunsGEM5=1
	export BBINARYS="$(echo $BINARYS | sed -E 's#(P/bin/dc\|A)#\1|1|24#g' | sed -E 's#(P/bin/dc\|B)#\1|1|32#g')"
fi
